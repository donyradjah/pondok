-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2017 at 07:33 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pondok_baru`
--

-- --------------------------------------------------------

--
-- Table structure for table `alat_transportasi`
--

CREATE TABLE `alat_transportasi` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `alat_transportasi`
--

INSERT INTO `alat_transportasi` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Jalan Kaki', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Sepeda', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Sepeda Motor', NULL, '2017-01-23 01:58:13', NULL, NULL, NULL, NULL),
(4, 'Mobil Pribadi', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Antar Jemput Sekolah', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Angkutan Umum', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Perahu/Sampan', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Lainnya', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `asal_sekolah`
--

CREATE TABLE `asal_sekolah` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `asal_sekolah`
--

INSERT INTO `asal_sekolah` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'MTS', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SMP', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'MA', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'SMA', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Paket A', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Paket B', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Pondok Pesantren', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `bidang_prestasi`
--

CREATE TABLE `bidang_prestasi` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(225) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bidang_prestasi`
--

INSERT INTO `bidang_prestasi` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Akademik, seperti : lomba Olimpiade Bidang Studi, Kompetisi Sains Madrasah, Cerdas Cermat, dll', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Keagamaan, seperti : lomba Baca/Hafalan Al Qur''an, Kaligrafi, Adzan, dll.', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Teknologi, seperti : lomba robotik, perancangan web atau sistem informasi, perakitan mesin, dll', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Olahraga, seperti : lomba bela diri, sepakbola, bola basket, bulutangkis, catur, tenis meja, bola voli, dll', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Pramuka/Paskribaka', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Karya Ilmiah Remaja', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Kesenian, seperti : lomba menyanyi, menari, melukis, alat musik, marching band, dll', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Pidato Bahasa Asing, seperti : lomba pidato bahasa inggris, bahasa mandarain, bahasa arab, dll', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Lainnya', '2017-01-23 03:21:36', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cita_cita`
--

CREATE TABLE `cita_cita` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cita_cita`
--

INSERT INTO `cita_cita` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'PNS', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'TNI/Polri', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Guru/Dosen', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Dokter', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Politikus', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Wiraswasta', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Pekerja Seni/Lukis/Artis/Sejenis', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Lainnya', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fuzzyoutput`
--

CREATE TABLE `fuzzyoutput` (
  `id` int(11) NOT NULL,
  `santri_id` int(11) DEFAULT NULL,
  `fo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fuzzyoutput`
--

INSERT INTO `fuzzyoutput` (`id`, `santri_id`, `fo`) VALUES
(1, 6, 'LAYAK'),
(2, 7, 'TIDAK LAYAK'),
(3, 8, 'LAYAK'),
(4, 8, 'LAYAK'),
(5, 9, 'TIDAK LAYAK'),
(6, 10, 'TIDAK LAYAK'),
(7, 11, 'LAYAK');

-- --------------------------------------------------------

--
-- Table structure for table `hoby`
--

CREATE TABLE `hoby` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hoby`
--

INSERT INTO `hoby` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Olahraga', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Kesenian', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Membaca', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Menulis', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Traveling', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Lainnya', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jarak`
--

CREATE TABLE `jarak` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jarak`
--

INSERT INTO `jarak` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, ' < 1 Km ', NULL, NULL, NULL, NULL, NULL, NULL),
(2, ' 1 - 3 Km	', NULL, NULL, NULL, NULL, NULL, NULL),
(3, ' 3 - 5 Km ', NULL, NULL, NULL, NULL, NULL, NULL),
(4, ' 5 - 10 Km ', NULL, NULL, NULL, NULL, NULL, NULL),
(5, ' > 10 Km ', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_pekerjaan`
--

CREATE TABLE `jenis_pekerjaan` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_pekerjaan`
--

INSERT INTO `jenis_pekerjaan` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Tidak bekerja (Di rumah saja) ', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Pensiunan/Almarhum ', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'PNS (selain poin 05 dan 10) ', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'TNI/Polisi ', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Guru/Dosen ', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Pegawai Swasta ', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Pengusaha/Wiraswasta ', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Pengacara/Hakim/Jaksa/Notaris ', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Seniman/Pelukis/Artis/Sejenis ', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'Dokter/Bidan/Perawat ', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'Pilot/Pramugari ', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'Pedagang ', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 'Petani/Peternak ', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 'Nelayan ', NULL, NULL, NULL, NULL, NULL, NULL),
(15, 'Buruh (Tani/Pabrik/Bangunan) ', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 'Sopir/Masinis/Kondektur ', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 'Politikus ', NULL, NULL, NULL, NULL, NULL, NULL),
(18, 'Lainnya ', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_sekolah`
--

CREATE TABLE `jenis_sekolah` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_sekolah`
--

INSERT INTO `jenis_sekolah` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'MI', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SD', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'MTS', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'SMP', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Paket A', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Paket B', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Pondok Pesantren', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jenjang_pendidikan`
--

CREATE TABLE `jenjang_pendidikan` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenjang_pendidikan`
--

INSERT INTO `jenjang_pendidikan` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'SLTP', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'SLTA', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'D1', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'D2', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'D3', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'D4', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'S1', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'S2', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'S3', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kota`
--

CREATE TABLE `kota` (
  `kota` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kota`
--

INSERT INTO `kota` (`kota`) VALUES
('KABUPATEN SIMEULUE'),
('KABUPATEN ACEH SINGKIL'),
('KABUPATEN ACEH SELATAN'),
('KABUPATEN ACEH TENGGARA'),
('KABUPATEN ACEH TIMUR'),
('KABUPATEN ACEH TENGAH'),
('KABUPATEN ACEH BARAT'),
('KABUPATEN ACEH BESAR'),
('KABUPATEN PIDIE'),
('KABUPATEN BIREUEN'),
('KABUPATEN ACEH UTARA'),
('KABUPATEN ACEH BARAT DAYA'),
('KABUPATEN GAYO LUES'),
('KABUPATEN ACEH TAMIANG'),
('KABUPATEN NAGAN RAYA'),
('KABUPATEN ACEH JAYA'),
('KABUPATEN BENER MERIAH'),
('KABUPATEN PIDIE JAYA'),
('KOTA BANDA ACEH'),
('KOTA SABANG'),
('KOTA LANGSA'),
('KOTA LHOKSEUMAWE'),
('KOTA SUBULUSSALAM'),
('KABUPATEN NIAS'),
('KABUPATEN MANDAILING NATAL'),
('KABUPATEN TAPANULI SELATAN'),
('KABUPATEN TAPANULI TENGAH'),
('KABUPATEN TAPANULI UTARA'),
('KABUPATEN TOBA SAMOSIR'),
('KABUPATEN LABUHAN BATU'),
('KABUPATEN ASAHAN'),
('KABUPATEN SIMALUNGUN'),
('KABUPATEN DAIRI'),
('KABUPATEN KARO'),
('KABUPATEN DELI SERDANG'),
('KABUPATEN LANGKAT'),
('KABUPATEN NIAS SELATAN'),
('KABUPATEN HUMBANG HASUNDUTAN'),
('KABUPATEN PAKPAK BHARAT'),
('KABUPATEN SAMOSIR'),
('KABUPATEN SERDANG BEDAGAI'),
('KABUPATEN BATU BARA'),
('KABUPATEN PADANG LAWAS UTARA'),
('KABUPATEN PADANG LAWAS'),
('KABUPATEN LABUHAN BATU SELATAN'),
('KABUPATEN LABUHAN BATU UTARA'),
('KABUPATEN NIAS UTARA'),
('KABUPATEN NIAS BARAT'),
('KOTA SIBOLGA'),
('KOTA TANJUNG BALAI'),
('KOTA PEMATANG SIANTAR'),
('KOTA TEBING TINGGI'),
('KOTA MEDAN'),
('KOTA BINJAI'),
('KOTA PADANGSIDIMPUAN'),
('KOTA GUNUNGSITOLI'),
('KABUPATEN KEPULAUAN MENTAWAI'),
('KABUPATEN PESISIR SELATAN'),
('KABUPATEN SOLOK'),
('KABUPATEN SIJUNJUNG'),
('KABUPATEN TANAH DATAR'),
('KABUPATEN PADANG PARIAMAN'),
('KABUPATEN AGAM'),
('KABUPATEN LIMA PULUH KOTA'),
('KABUPATEN PASAMAN'),
('KABUPATEN SOLOK SELATAN'),
('KABUPATEN DHARMASRAYA'),
('KABUPATEN PASAMAN BARAT'),
('KOTA PADANG'),
('KOTA SOLOK'),
('KOTA SAWAH LUNTO'),
('KOTA PADANG PANJANG'),
('KOTA BUKITTINGGI'),
('KOTA PAYAKUMBUH'),
('KOTA PARIAMAN'),
('KABUPATEN KUANTAN SINGINGI'),
('KABUPATEN INDRAGIRI HULU'),
('KABUPATEN INDRAGIRI HILIR'),
('KABUPATEN PELALAWAN'),
('KABUPATEN S I A K'),
('KABUPATEN KAMPAR'),
('KABUPATEN ROKAN HULU'),
('KABUPATEN BENGKALIS'),
('KABUPATEN ROKAN HILIR'),
('KABUPATEN KEPULAUAN MERANTI'),
('KOTA PEKANBARU'),
('KOTA D U M A I'),
('KABUPATEN KERINCI'),
('KABUPATEN MERANGIN'),
('KABUPATEN SAROLANGUN'),
('KABUPATEN BATANG HARI'),
('KABUPATEN MUARO JAMBI'),
('KABUPATEN TANJUNG JABUNG TIMUR'),
('KABUPATEN TANJUNG JABUNG BARAT'),
('KABUPATEN TEBO'),
('KABUPATEN BUNGO'),
('KOTA JAMBI'),
('KOTA SUNGAI PENUH'),
('KABUPATEN OGAN KOMERING ULU'),
('KABUPATEN OGAN KOMERING ILIR'),
('KABUPATEN MUARA ENIM'),
('KABUPATEN LAHAT'),
('KABUPATEN MUSI RAWAS'),
('KABUPATEN MUSI BANYUASIN'),
('KABUPATEN BANYU ASIN'),
('KABUPATEN OGAN KOMERING ULU SELATAN'),
('KABUPATEN OGAN KOMERING ULU TIMUR'),
('KABUPATEN OGAN ILIR'),
('KABUPATEN EMPAT LAWANG'),
('KABUPATEN PENUKAL ABAB LEMATANG ILIR'),
('KABUPATEN MUSI RAWAS UTARA'),
('KOTA PALEMBANG'),
('KOTA PRABUMULIH'),
('KOTA PAGAR ALAM'),
('KOTA LUBUKLINGGAU'),
('KABUPATEN BENGKULU SELATAN'),
('KABUPATEN REJANG LEBONG'),
('KABUPATEN BENGKULU UTARA'),
('KABUPATEN KAUR'),
('KABUPATEN SELUMA'),
('KABUPATEN MUKOMUKO'),
('KABUPATEN LEBONG'),
('KABUPATEN KEPAHIANG'),
('KABUPATEN BENGKULU TENGAH'),
('KOTA BENGKULU'),
('KABUPATEN LAMPUNG BARAT'),
('KABUPATEN TANGGAMUS'),
('KABUPATEN LAMPUNG SELATAN'),
('KABUPATEN LAMPUNG TIMUR'),
('KABUPATEN LAMPUNG TENGAH'),
('KABUPATEN LAMPUNG UTARA'),
('KABUPATEN WAY KANAN'),
('KABUPATEN TULANGBAWANG'),
('KABUPATEN PESAWARAN'),
('KABUPATEN PRINGSEWU'),
('KABUPATEN MESUJI'),
('KABUPATEN TULANG BAWANG BARAT'),
('KABUPATEN PESISIR BARAT'),
('KOTA BANDAR LAMPUNG'),
('KOTA METRO'),
('KABUPATEN BANGKA'),
('KABUPATEN BELITUNG'),
('KABUPATEN BANGKA BARAT'),
('KABUPATEN BANGKA TENGAH'),
('KABUPATEN BANGKA SELATAN'),
('KABUPATEN BELITUNG TIMUR'),
('KOTA PANGKAL PINANG'),
('KABUPATEN KARIMUN'),
('KABUPATEN BINTAN'),
('KABUPATEN NATUNA'),
('KABUPATEN LINGGA'),
('KABUPATEN KEPULAUAN ANAMBAS'),
('KOTA B A T A M'),
('KOTA TANJUNG PINANG'),
('KABUPATEN KEPULAUAN SERIBU'),
('KOTA JAKARTA SELATAN'),
('KOTA JAKARTA TIMUR'),
('KOTA JAKARTA PUSAT'),
('KOTA JAKARTA BARAT'),
('KOTA JAKARTA UTARA'),
('KABUPATEN BOGOR'),
('KABUPATEN SUKABUMI'),
('KABUPATEN CIANJUR'),
('KABUPATEN BANDUNG'),
('KABUPATEN GARUT'),
('KABUPATEN TASIKMALAYA'),
('KABUPATEN CIAMIS'),
('KABUPATEN KUNINGAN'),
('KABUPATEN CIREBON'),
('KABUPATEN MAJALENGKA'),
('KABUPATEN SUMEDANG'),
('KABUPATEN INDRAMAYU'),
('KABUPATEN SUBANG'),
('KABUPATEN PURWAKARTA'),
('KABUPATEN KARAWANG'),
('KABUPATEN BEKASI'),
('KABUPATEN BANDUNG BARAT'),
('KABUPATEN PANGANDARAN'),
('KOTA BOGOR'),
('KOTA SUKABUMI'),
('KOTA BANDUNG'),
('KOTA CIREBON'),
('KOTA BEKASI'),
('KOTA DEPOK'),
('KOTA CIMAHI'),
('KOTA TASIKMALAYA'),
('KOTA BANJAR'),
('KABUPATEN CILACAP'),
('KABUPATEN BANYUMAS'),
('KABUPATEN PURBALINGGA'),
('KABUPATEN BANJARNEGARA'),
('KABUPATEN KEBUMEN'),
('KABUPATEN PURWOREJO'),
('KABUPATEN WONOSOBO'),
('KABUPATEN MAGELANG'),
('KABUPATEN BOYOLALI'),
('KABUPATEN KLATEN'),
('KABUPATEN SUKOHARJO'),
('KABUPATEN WONOGIRI'),
('KABUPATEN KARANGANYAR'),
('KABUPATEN SRAGEN'),
('KABUPATEN GROBOGAN'),
('KABUPATEN BLORA'),
('KABUPATEN REMBANG'),
('KABUPATEN PATI'),
('KABUPATEN KUDUS'),
('KABUPATEN JEPARA'),
('KABUPATEN DEMAK'),
('KABUPATEN SEMARANG'),
('KABUPATEN TEMANGGUNG'),
('KABUPATEN KENDAL'),
('KABUPATEN BATANG'),
('KABUPATEN PEKALONGAN'),
('KABUPATEN PEMALANG'),
('KABUPATEN TEGAL'),
('KABUPATEN BREBES'),
('KOTA MAGELANG'),
('KOTA SURAKARTA'),
('KOTA SALATIGA'),
('KOTA SEMARANG'),
('KOTA PEKALONGAN'),
('KOTA TEGAL'),
('KABUPATEN KULON PROGO'),
('KABUPATEN BANTUL'),
('KABUPATEN GUNUNG KIDUL'),
('KABUPATEN SLEMAN'),
('KOTA YOGYAKARTA'),
('KABUPATEN PACITAN'),
('KABUPATEN PONOROGO'),
('KABUPATEN TRENGGALEK'),
('KABUPATEN TULUNGAGUNG'),
('KABUPATEN BLITAR'),
('KABUPATEN KEDIRI'),
('KABUPATEN MALANG'),
('KABUPATEN LUMAJANG'),
('KABUPATEN JEMBER'),
('KABUPATEN BANYUWANGI'),
('KABUPATEN BONDOWOSO'),
('KABUPATEN SITUBONDO'),
('KABUPATEN PROBOLINGGO'),
('KABUPATEN PASURUAN'),
('KABUPATEN SIDOARJO'),
('KABUPATEN MOJOKERTO'),
('KABUPATEN JOMBANG'),
('KABUPATEN NGANJUK'),
('KABUPATEN MADIUN'),
('KABUPATEN MAGETAN'),
('KABUPATEN NGAWI'),
('KABUPATEN BOJONEGORO'),
('KABUPATEN TUBAN'),
('KABUPATEN LAMONGAN'),
('KABUPATEN GRESIK'),
('KABUPATEN BANGKALAN'),
('KABUPATEN SAMPANG'),
('KABUPATEN PAMEKASAN'),
('KABUPATEN SUMENEP'),
('KOTA KEDIRI'),
('KOTA BLITAR'),
('KOTA MALANG'),
('KOTA PROBOLINGGO'),
('KOTA PASURUAN'),
('KOTA MOJOKERTO'),
('KOTA MADIUN'),
('KOTA SURABAYA'),
('KOTA BATU'),
('KABUPATEN PANDEGLANG'),
('KABUPATEN LEBAK'),
('KABUPATEN TANGERANG'),
('KABUPATEN SERANG'),
('KOTA TANGERANG'),
('KOTA CILEGON'),
('KOTA SERANG'),
('KOTA TANGERANG SELATAN'),
('KABUPATEN JEMBRANA'),
('KABUPATEN TABANAN'),
('KABUPATEN BADUNG'),
('KABUPATEN GIANYAR'),
('KABUPATEN KLUNGKUNG'),
('KABUPATEN BANGLI'),
('KABUPATEN KARANG ASEM'),
('KABUPATEN BULELENG'),
('KOTA DENPASAR'),
('KABUPATEN LOMBOK BARAT'),
('KABUPATEN LOMBOK TENGAH'),
('KABUPATEN LOMBOK TIMUR'),
('KABUPATEN SUMBAWA'),
('KABUPATEN DOMPU'),
('KABUPATEN BIMA'),
('KABUPATEN SUMBAWA BARAT'),
('KABUPATEN LOMBOK UTARA'),
('KOTA MATARAM'),
('KOTA BIMA'),
('KABUPATEN SUMBA BARAT'),
('KABUPATEN SUMBA TIMUR'),
('KABUPATEN KUPANG'),
('KABUPATEN TIMOR TENGAH SELATAN'),
('KABUPATEN TIMOR TENGAH UTARA'),
('KABUPATEN BELU'),
('KABUPATEN ALOR'),
('KABUPATEN LEMBATA'),
('KABUPATEN FLORES TIMUR'),
('KABUPATEN SIKKA'),
('KABUPATEN ENDE'),
('KABUPATEN NGADA'),
('KABUPATEN MANGGARAI'),
('KABUPATEN ROTE NDAO'),
('KABUPATEN MANGGARAI BARAT'),
('KABUPATEN SUMBA TENGAH'),
('KABUPATEN SUMBA BARAT DAYA'),
('KABUPATEN NAGEKEO'),
('KABUPATEN MANGGARAI TIMUR'),
('KABUPATEN SABU RAIJUA'),
('KABUPATEN MALAKA'),
('KOTA KUPANG'),
('KABUPATEN SAMBAS'),
('KABUPATEN BENGKAYANG'),
('KABUPATEN LANDAK'),
('KABUPATEN MEMPAWAH'),
('KABUPATEN SANGGAU'),
('KABUPATEN KETAPANG'),
('KABUPATEN SINTANG'),
('KABUPATEN KAPUAS HULU'),
('KABUPATEN SEKADAU'),
('KABUPATEN MELAWI'),
('KABUPATEN KAYONG UTARA'),
('KABUPATEN KUBU RAYA'),
('KOTA PONTIANAK'),
('KOTA SINGKAWANG'),
('KABUPATEN KOTAWARINGIN BARAT'),
('KABUPATEN KOTAWARINGIN TIMUR'),
('KABUPATEN KAPUAS'),
('KABUPATEN BARITO SELATAN'),
('KABUPATEN BARITO UTARA'),
('KABUPATEN SUKAMARA'),
('KABUPATEN LAMANDAU'),
('KABUPATEN SERUYAN'),
('KABUPATEN KATINGAN'),
('KABUPATEN PULANG PISAU'),
('KABUPATEN GUNUNG MAS'),
('KABUPATEN BARITO TIMUR'),
('KABUPATEN MURUNG RAYA'),
('KOTA PALANGKA RAYA'),
('KABUPATEN TANAH LAUT'),
('KABUPATEN KOTA BARU'),
('KABUPATEN BANJAR'),
('KABUPATEN BARITO KUALA'),
('KABUPATEN TAPIN'),
('KABUPATEN HULU SUNGAI SELATAN'),
('KABUPATEN HULU SUNGAI TENGAH'),
('KABUPATEN HULU SUNGAI UTARA'),
('KABUPATEN TABALONG'),
('KABUPATEN TANAH BUMBU'),
('KABUPATEN BALANGAN'),
('KOTA BANJARMASIN'),
('KOTA BANJAR BARU'),
('KABUPATEN PASER'),
('KABUPATEN KUTAI BARAT'),
('KABUPATEN KUTAI KARTANEGARA'),
('KABUPATEN KUTAI TIMUR'),
('KABUPATEN BERAU'),
('KABUPATEN PENAJAM PASER UTARA'),
('KABUPATEN MAHAKAM HULU'),
('KOTA BALIKPAPAN'),
('KOTA SAMARINDA'),
('KOTA BONTANG'),
('KABUPATEN MALINAU'),
('KABUPATEN BULUNGAN'),
('KABUPATEN TANA TIDUNG'),
('KABUPATEN NUNUKAN'),
('KOTA TARAKAN'),
('KABUPATEN BOLAANG MONGONDOW'),
('KABUPATEN MINAHASA'),
('KABUPATEN KEPULAUAN SANGIHE'),
('KABUPATEN KEPULAUAN TALAUD'),
('KABUPATEN MINAHASA SELATAN'),
('KABUPATEN MINAHASA UTARA'),
('KABUPATEN BOLAANG MONGONDOW UTARA'),
('KABUPATEN SIAU TAGULANDANG BIARO'),
('KABUPATEN MINAHASA TENGGARA'),
('KABUPATEN BOLAANG MONGONDOW SELATAN'),
('KABUPATEN BOLAANG MONGONDOW TIMUR'),
('KOTA MANADO'),
('KOTA BITUNG'),
('KOTA TOMOHON'),
('KOTA KOTAMOBAGU'),
('KABUPATEN BANGGAI KEPULAUAN'),
('KABUPATEN BANGGAI'),
('KABUPATEN MOROWALI'),
('KABUPATEN POSO'),
('KABUPATEN DONGGALA'),
('KABUPATEN TOLI-TOLI'),
('KABUPATEN BUOL'),
('KABUPATEN PARIGI MOUTONG'),
('KABUPATEN TOJO UNA-UNA'),
('KABUPATEN SIGI'),
('KABUPATEN BANGGAI LAUT'),
('KABUPATEN MOROWALI UTARA'),
('KOTA PALU'),
('KABUPATEN KEPULAUAN SELAYAR'),
('KABUPATEN BULUKUMBA'),
('KABUPATEN BANTAENG'),
('KABUPATEN JENEPONTO'),
('KABUPATEN TAKALAR'),
('KABUPATEN GOWA'),
('KABUPATEN SINJAI'),
('KABUPATEN MAROS'),
('KABUPATEN PANGKAJENE DAN KEPULAUAN'),
('KABUPATEN BARRU'),
('KABUPATEN BONE'),
('KABUPATEN SOPPENG'),
('KABUPATEN WAJO'),
('KABUPATEN SIDENRENG RAPPANG'),
('KABUPATEN PINRANG'),
('KABUPATEN ENREKANG'),
('KABUPATEN LUWU'),
('KABUPATEN TANA TORAJA'),
('KABUPATEN LUWU UTARA'),
('KABUPATEN LUWU TIMUR'),
('KABUPATEN TORAJA UTARA'),
('KOTA MAKASSAR'),
('KOTA PAREPARE'),
('KOTA PALOPO'),
('KABUPATEN BUTON'),
('KABUPATEN MUNA'),
('KABUPATEN KONAWE'),
('KABUPATEN KOLAKA'),
('KABUPATEN KONAWE SELATAN'),
('KABUPATEN BOMBANA'),
('KABUPATEN WAKATOBI'),
('KABUPATEN KOLAKA UTARA'),
('KABUPATEN BUTON UTARA'),
('KABUPATEN KONAWE UTARA'),
('KABUPATEN KOLAKA TIMUR'),
('KABUPATEN KONAWE KEPULAUAN'),
('KABUPATEN MUNA BARAT'),
('KABUPATEN BUTON TENGAH'),
('KABUPATEN BUTON SELATAN'),
('KOTA KENDARI'),
('KOTA BAUBAU'),
('KABUPATEN BOALEMO'),
('KABUPATEN GORONTALO'),
('KABUPATEN POHUWATO'),
('KABUPATEN BONE BOLANGO'),
('KABUPATEN GORONTALO UTARA'),
('KOTA GORONTALO'),
('KABUPATEN MAJENE'),
('KABUPATEN POLEWALI MANDAR'),
('KABUPATEN MAMASA'),
('KABUPATEN MAMUJU'),
('KABUPATEN MAMUJU UTARA'),
('KABUPATEN MAMUJU TENGAH'),
('KABUPATEN MALUKU TENGGARA BARAT'),
('KABUPATEN MALUKU TENGGARA'),
('KABUPATEN MALUKU TENGAH'),
('KABUPATEN BURU'),
('KABUPATEN KEPULAUAN ARU'),
('KABUPATEN SERAM BAGIAN BARAT'),
('KABUPATEN SERAM BAGIAN TIMUR'),
('KABUPATEN MALUKU BARAT DAYA'),
('KABUPATEN BURU SELATAN'),
('KOTA AMBON'),
('KOTA TUAL'),
('KABUPATEN HALMAHERA BARAT'),
('KABUPATEN HALMAHERA TENGAH'),
('KABUPATEN KEPULAUAN SULA'),
('KABUPATEN HALMAHERA SELATAN'),
('KABUPATEN HALMAHERA UTARA'),
('KABUPATEN HALMAHERA TIMUR'),
('KABUPATEN PULAU MOROTAI'),
('KABUPATEN PULAU TALIABU'),
('KOTA TERNATE'),
('KOTA TIDORE KEPULAUAN'),
('KABUPATEN FAKFAK'),
('KABUPATEN KAIMANA'),
('KABUPATEN TELUK WONDAMA'),
('KABUPATEN TELUK BINTUNI'),
('KABUPATEN MANOKWARI'),
('KABUPATEN SORONG SELATAN'),
('KABUPATEN SORONG'),
('KABUPATEN RAJA AMPAT'),
('KABUPATEN TAMBRAUW'),
('KABUPATEN MAYBRAT'),
('KABUPATEN MANOKWARI SELATAN'),
('KABUPATEN PEGUNUNGAN ARFAK'),
('KOTA SORONG'),
('KABUPATEN MERAUKE'),
('KABUPATEN JAYAWIJAYA'),
('KABUPATEN JAYAPURA'),
('KABUPATEN NABIRE'),
('KABUPATEN KEPULAUAN YAPEN'),
('KABUPATEN BIAK NUMFOR'),
('KABUPATEN PANIAI'),
('KABUPATEN PUNCAK JAYA'),
('KABUPATEN MIMIKA'),
('KABUPATEN BOVEN DIGOEL'),
('KABUPATEN MAPPI'),
('KABUPATEN ASMAT'),
('KABUPATEN YAHUKIMO'),
('KABUPATEN PEGUNUNGAN BINTANG'),
('KABUPATEN TOLIKARA'),
('KABUPATEN SARMI'),
('KABUPATEN KEEROM'),
('KABUPATEN WAROPEN'),
('KABUPATEN SUPIORI'),
('KABUPATEN MAMBERAMO RAYA'),
('KABUPATEN NDUGA'),
('KABUPATEN LANNY JAYA'),
('KABUPATEN MAMBERAMO TENGAH'),
('KABUPATEN YALIMO'),
('KABUPATEN PUNCAK'),
('KABUPATEN DOGIYAI'),
('KABUPATEN INTAN JAYA'),
('KABUPATEN DEIYAI'),
('KOTA JAYAPURA');

-- --------------------------------------------------------

--
-- Table structure for table `kriteria_gap`
--

CREATE TABLE `kriteria_gap` (
  `id` int(11) NOT NULL,
  `adzan` double(11,5) DEFAULT NULL,
  `sholat` double(11,5) DEFAULT NULL,
  `quran` double(11,5) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kriteria_gap`
--

INSERT INTO `kriteria_gap` (`id`, `adzan`, `sholat`, `quran`) VALUES
(1, 66.60000, 66.60000, 66.60000);

-- --------------------------------------------------------

--
-- Table structure for table `nilai`
--

CREATE TABLE `nilai` (
  `id` int(11) NOT NULL,
  `santri_id` int(11) DEFAULT NULL,
  `quran` double(10,2) DEFAULT NULL,
  `sholat` double(10,2) DEFAULT NULL,
  `adzan` double(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nilai`
--

INSERT INTO `nilai` (`id`, `santri_id`, `quran`, `sholat`, `adzan`) VALUES
(1, 6, 60.00, 60.00, 60.00),
(2, 8, 89.00, 70.00, 78.00),
(3, 7, 30.00, 40.00, 40.00),
(4, 8, 89.00, 70.00, 78.00),
(5, 9, 30.00, 30.00, 30.00),
(6, 10, 0.00, 0.00, 0.00),
(7, 11, 80.00, 60.00, 66.00);

-- --------------------------------------------------------

--
-- Table structure for table `penghasilan`
--

CREATE TABLE `penghasilan` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penghasilan`
--

INSERT INTO `penghasilan` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, '<=  Rp 500.000', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Rp 500.001 - Rp 1.000.000', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Rp 1.000.001 - Rp 2.000.000', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Rp 2.000.001 - Rp 3.000.000', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Rp 3.000.001 - Rp 5.000.000', NULL, NULL, NULL, NULL, NULL, NULL),
(6, '> Rp 5.000.000', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `peringkat_prestasi`
--

CREATE TABLE `peringkat_prestasi` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `peringkat_prestasi`
--

INSERT INTO `peringkat_prestasi` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Juara I', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Juara II', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Juara III', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Juara Harapan I', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Juara Harapan II', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Juara Harapan III', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Juara Favorit', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `provinsi`
--

CREATE TABLE `provinsi` (
  `provinsi` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `provinsi`
--

INSERT INTO `provinsi` (`provinsi`) VALUES
('ACEH'),
('SUMATERA UTARA'),
('SUMATERA BARAT'),
('RIAU'),
('JAMBI'),
('SUMATERA SELATAN'),
('BENGKULU'),
('LAMPUNG'),
('KEPULAUAN BANGKA BELITUNG'),
('KEPULAUAN RIAU'),
('DKI JAKARTA'),
('JAWA BARAT'),
('JAWA TENGAH'),
('DI YOGYAKARTA'),
('JAWA TIMUR'),
('BANTEN'),
('BALI'),
('NUSA TENGGARA BARAT'),
('NUSA TENGGARA TIMUR'),
('KALIMANTAN BARAT'),
('KALIMANTAN TENGAH'),
('KALIMANTAN SELATAN'),
('KALIMANTAN TIMUR'),
('KALIMANTAN UTARA'),
('SULAWESI UTARA'),
('SULAWESI TENGAH'),
('SULAWESI SELATAN'),
('SULAWESI TENGGARA'),
('GORONTALO'),
('SULAWESI BARAT'),
('MALUKU'),
('MALUKU UTARA'),
('PAPUA BARAT'),
('PAPUA');

-- --------------------------------------------------------

--
-- Table structure for table `range_bobot`
--

CREATE TABLE `range_bobot` (
  `id` int(11) NOT NULL,
  `range_kecil` double DEFAULT NULL,
  `range_besar` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `range_bobot`
--

INSERT INTO `range_bobot` (`id`, `range_kecil`, `range_besar`) VALUES
(1, 0, 20),
(2, 21, 59),
(3, 60, 70),
(4, 71, 80),
(5, 81, 100);

-- --------------------------------------------------------

--
-- Table structure for table `ranking`
--

CREATE TABLE `ranking` (
  `id` int(11) NOT NULL,
  `nilai` double(11,5) DEFAULT NULL,
  `santri_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ranking`
--

INSERT INTO `ranking` (`id`, `nilai`, `santri_id`) VALUES
(1, 2.00000, 6),
(2, 3.15000, 8),
(3, 3.15000, 8),
(4, 2.30000, 11);

-- --------------------------------------------------------

--
-- Table structure for table `rulebase`
--

CREATE TABLE `rulebase` (
  `id` int(11) NOT NULL,
  `fo` varchar(50) DEFAULT NULL,
  `quran` int(10) DEFAULT NULL,
  `sholat` int(10) DEFAULT NULL,
  `adzan` int(10) DEFAULT NULL,
  `nilai` double(5,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rulebase`
--

INSERT INTO `rulebase` (`id`, `fo`, `quran`, `sholat`, `adzan`, `nilai`) VALUES
(1, 'TIDAK LAYAK', 1, 1, 1, 60),
(2, 'TIDAK LAYAK', 1, 1, 2, 60),
(3, 'TIDAK LAYAK', 1, 1, 3, 60),
(4, 'TIDAK LAYAK', 1, 2, 1, 60),
(5, 'TIDAK LAYAK', 1, 2, 2, 60),
(6, 'TIDAK LAYAK', 1, 2, 3, 60),
(7, 'TIDAK LAYAK', 1, 3, 1, 60),
(8, 'LAYAK', 1, 3, 2, 100),
(9, 'LAYAK', 1, 3, 3, 100),
(10, 'TIDAK LAYAK', 2, 1, 1, 60),
(11, 'TIDAK LAYAK', 2, 1, 2, 60),
(12, 'LAYAK', 2, 1, 3, 100),
(13, 'TIDAK LAYAK', 2, 2, 1, 60),
(14, 'LAYAK', 2, 2, 2, 100),
(15, 'LAYAK', 2, 2, 3, 100),
(16, 'TIDAK LAYAK', 2, 3, 1, 60),
(17, 'LAYAK', 2, 3, 2, 100),
(18, 'LAYAK', 2, 3, 3, 100),
(19, 'TIDAK LAYAK', 3, 1, 1, 60),
(20, 'LAYAK', 3, 1, 2, 100),
(21, 'LAYAK', 3, 1, 3, 100),
(22, 'TIDAK LAYAK', 3, 2, 1, 60),
(23, 'LAYAK', 3, 2, 2, 100),
(24, 'LAYAK', 3, 2, 3, 100),
(25, 'TIDAK LAYAK', 3, 3, 1, 60),
(26, 'LAYAK', 3, 3, 2, 100),
(27, 'LAYAK', 3, 3, 3, 100);

-- --------------------------------------------------------

--
-- Table structure for table `santri`
--

CREATE TABLE `santri` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `status_lembaga` bigint(20) DEFAULT NULL,
  `NIS` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NISN` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NIK` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tempat_lahir` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tanggal_lahir` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_kelamin` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `hoby` bigint(20) DEFAULT NULL,
  `cita_cita` bigint(20) DEFAULT NULL,
  `jumlah_saudara` bigint(20) DEFAULT NULL,
  `status_siswa` bigint(20) DEFAULT NULL,
  `asal_sekolah` bigint(20) DEFAULT NULL,
  `jenis_sekolah` bigint(20) DEFAULT NULL,
  `status_sekolah` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_kabupaten_sekolah` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_skhun` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NPSN` bigint(50) DEFAULT NULL,
  `nomor_blanko_skhun` varchar(20) DEFAULT NULL,
  `nomor_ijazah` varchar(20) DEFAULT NULL,
  `total_nilai` double(10,5) DEFAULT NULL,
  `tanggal_lulus` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `alamat_ortu` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `provinsi_ortu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_kabupaten_ortu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_kecamatan_ortu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_desa_ortu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `kode_pos_ortu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jarak` bigint(20) DEFAULT NULL,
  `alat_transportasi` bigint(20) DEFAULT NULL,
  `kebutuhan_khusus` tinyint(1) DEFAULT NULL,
  `tuna_rungu` tinyint(1) DEFAULT NULL,
  `tuna_netra` tinyint(1) DEFAULT NULL,
  `tuna_daksa` tinyint(1) DEFAULT NULL,
  `tuna_grahita` tinyint(1) DEFAULT NULL,
  `tuna_laras` tinyint(1) DEFAULT NULL,
  `lamban_belajar` tinyint(1) DEFAULT NULL,
  `sulit_belajar` tinyint(1) DEFAULT NULL,
  `gangguan_komunikasi` tinyint(1) DEFAULT NULL,
  `bakat_luar_biasa` tinyint(1) DEFAULT NULL,
  `nomor_kk` bigint(20) DEFAULT NULL,
  `nama_ayah` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NIK_ayah` bigint(20) DEFAULT NULL,
  `jenjang_pendidikan_ayah` bigint(20) DEFAULT NULL,
  `jenis_pekerjaan_ayah` bigint(20) DEFAULT NULL,
  `no_hp_ayah` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nama_ibu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NIK_ibu` bigint(20) DEFAULT NULL,
  `jenjang_pendidikan_ibu` bigint(20) DEFAULT NULL,
  `jenis_pekerjaan_ibu` bigint(20) DEFAULT NULL,
  `no_hp_ibu` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `rata_penghasilan_ortu` bigint(20) DEFAULT NULL,
  `nama_wali` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `NIK_wali` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tahun_lahir_wali` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenjang_pendidikan_wali` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `jenis_pekerjaan_wali` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `status_wali` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_hp_wali` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `rata_penghasilan_wali` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `bidang_prestasi` bigint(20) DEFAULT NULL,
  `tingkat_prestasi` bigint(20) DEFAULT NULL,
  `peringkat_prestasi` bigint(20) DEFAULT NULL,
  `tahun_prestasi` bigint(20) DEFAULT NULL,
  `nomor_kks` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_pkh` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomor_kip` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT '0',
  `updated_by` bigint(20) UNSIGNED DEFAULT '0',
  `deleted_by` bigint(20) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `tahunmasuk` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `santri`
--

INSERT INTO `santri` (`id`, `status_lembaga`, `NIS`, `NISN`, `NIK`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `hoby`, `cita_cita`, `jumlah_saudara`, `status_siswa`, `asal_sekolah`, `jenis_sekolah`, `status_sekolah`, `nama_kabupaten_sekolah`, `nomor_skhun`, `NPSN`, `nomor_blanko_skhun`, `nomor_ijazah`, `total_nilai`, `tanggal_lulus`, `alamat_ortu`, `provinsi_ortu`, `nama_kabupaten_ortu`, `nama_kecamatan_ortu`, `nama_desa_ortu`, `kode_pos_ortu`, `jarak`, `alat_transportasi`, `kebutuhan_khusus`, `tuna_rungu`, `tuna_netra`, `tuna_daksa`, `tuna_grahita`, `tuna_laras`, `lamban_belajar`, `sulit_belajar`, `gangguan_komunikasi`, `bakat_luar_biasa`, `nomor_kk`, `nama_ayah`, `NIK_ayah`, `jenjang_pendidikan_ayah`, `jenis_pekerjaan_ayah`, `no_hp_ayah`, `nama_ibu`, `NIK_ibu`, `jenjang_pendidikan_ibu`, `jenis_pekerjaan_ibu`, `no_hp_ibu`, `rata_penghasilan_ortu`, `nama_wali`, `NIK_wali`, `tahun_lahir_wali`, `jenjang_pendidikan_wali`, `jenis_pekerjaan_wali`, `status_wali`, `no_hp_wali`, `rata_penghasilan_wali`, `bidang_prestasi`, `tingkat_prestasi`, `peringkat_prestasi`, `tahun_prestasi`, `nomor_kks`, `nomor_pkh`, `nomor_kip`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`, `tahunmasuk`) VALUES
(6, 1, '1', '', '2222222222222223', 'AHMAD FAUZAN', 'KABUPATEN ACEH BARAT', '23/01/2017', 'L', 2, 5, 0, 1, 0, 0, '', '', '', 44454545, '', '', 0.00000, '', '', '', '', '', '', '', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '', 2017, 1, NULL, '0000-00-00 00:00:00', '2017-02-04 02:28:24', NULL, 17),
(7, 3, '1', '', '2222222222222222', 'AHMAD MUHYIDIN', 'KABUPATEN ALOR', '16/01/2017', 'L', 4, 7, 0, 3, 0, 0, '', '', '', 0, '', '', 0.00000, '', '', '', '', '', '', '', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '', 2017, 1, NULL, '0000-00-00 00:00:00', '2017-02-04 02:30:03', NULL, 17),
(8, 3, '2', '0000898989', '1234567891234567', 'MUHAMAD ARDHAN', 'KABUPATEN ACEH SELATAN', '29/01/2001', 'L', 3, 4, 4, 3, 0, 0, '', '', '', 0, '', '', 0.00000, '', '', '', '', '', '', '', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '', 2017, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, 17),
(9, 1, '2', '1234567867', '1234567891234555', 'ERGI SAPUTRA', 'KABUPATEN ACEH BARAT DAYA', '12/11/2000', 'L', 3, 0, 0, 5, 0, 0, '', '', '', 0, '', '', 0.00000, '', '', '', '', '', '', '', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '', 2017, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, 17),
(10, 2, '1', '2121343234', '1213324567123456', 'AHMAD MUJTABA FARHAN', 'KABUPATEN ALOR', '21/02/2000', 'L', 2, 0, 0, 5, 0, 0, '', '', '', 0, '', '', 0.00000, '', '', '', '', '', '', '', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '', 2017, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, 17),
(11, 1, '3', '0989898989', '1234567891234567', 'AHMAD ', 'KOTA PASURUAN', '10/07/2007', 'L', 1, 5, 2, 5, 0, 0, '', '', '', 0, '', '', 0.00000, '', '', '', '', '', '', '', 0, NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 0, 0, '', '', 0, 0, 0, '', 0, '', '', '', '', '', '', '', '', 0, 0, 0, 0, '', '', '', 2017, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL, 17);

-- --------------------------------------------------------

--
-- Table structure for table `selisih_gap`
--

CREATE TABLE `selisih_gap` (
  `id` int(11) NOT NULL,
  `selisih` varchar(50) DEFAULT NULL,
  `bobot` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selisih_gap`
--

INSERT INTO `selisih_gap` (`id`, `selisih`, `bobot`) VALUES
(1, '0', 4),
(2, '1', 4.5),
(3, '-1', 3.5),
(4, '2', 5),
(5, '-2', 3),
(6, '3', 5.5),
(7, '-3', 2.5),
(8, '4', 6),
(9, '-4', 2);

-- --------------------------------------------------------

--
-- Table structure for table `status_lembaga`
--

CREATE TABLE `status_lembaga` (
  `id` int(11) NOT NULL,
  `nama` varchar(35) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_lembaga`
--

INSERT INTO `status_lembaga` (`id`, `nama`, `kode`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Madrasah Aliyah', 'MA', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Madrasah Tsanawiyah', 'MTS', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Madrasah Diniyah', 'MD', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_siswa`
--

CREATE TABLE `status_siswa` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_siswa`
--

INSERT INTO `status_siswa` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Naik dari tingkat sebelumnya', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Mengulang (tidak naik kelas)', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Siswa pindah/mutasi masuk', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Drop-out kembali', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Siswa baru', NULL, '2017-02-02 03:26:37', NULL, NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `status_wali`
--

CREATE TABLE `status_wali` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `status_wali`
--

INSERT INTO `status_wali` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Kakek', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Nenek', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Paman', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Bibi', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 'Ibu Tiri', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Ayah Tiri', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'Kakak', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'Saudara', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'Lainnya', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tingkat_prestasi`
--

CREATE TABLE `tingkat_prestasi` (
  `id` int(11) NOT NULL,
  `deskripsi` varchar(35) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tingkat_prestasi`
--

INSERT INTO `tingkat_prestasi` (`id`, `deskripsi`, `created_at`, `updated_at`, `deleted_at`, `created_by`, `updated_by`, `deleted_by`) VALUES
(1, 'Tingkat Kabupaten/Kota', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Tingkat Provinsi', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'Tingkat Nasional', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'Tingkat Internasional', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT '0',
  `updated_by` bigint(20) UNSIGNED DEFAULT '0',
  `deleted_by` bigint(20) UNSIGNED DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `email`, `password`, `level`, `created_by`, `updated_by`, `deleted_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'admin 1', 'admin1@pondok.app', '$2y$12$3zWSWmhV4RsCu9GNmBP55.xiK8GkbEj715rNqngkKCDOUAdzOY7g2', 200, 0, 0, 0, NULL, '2017-01-24 22:43:23', NULL),
(2, 'admin 2', 'admin2@pondok.app', '$2y$12$3zWSWmhV4RsCu9GNmBP55.xiK8GkbEj715rNqngkKCDOUAdzOY7g2', 100, 0, 0, 0, NULL, '2017-01-24 22:44:01', NULL),
(4, 'esa', 'badal@gmail.com', '$2y$12$3d74Cb2d5.xQ5EsNgT5YEeSXNJ2Irkw/P4ofo37tV6RXWRG9IjpHy', 100, 2017, NULL, NULL, '0000-00-00 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `alat_transportasi`
--
ALTER TABLE `alat_transportasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `asal_sekolah`
--
ALTER TABLE `asal_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bidang_prestasi`
--
ALTER TABLE `bidang_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cita_cita`
--
ALTER TABLE `cita_cita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fuzzyoutput`
--
ALTER TABLE `fuzzyoutput`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hoby`
--
ALTER TABLE `hoby`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jarak`
--
ALTER TABLE `jarak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_pekerjaan`
--
ALTER TABLE `jenis_pekerjaan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenis_sekolah`
--
ALTER TABLE `jenis_sekolah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kriteria_gap`
--
ALTER TABLE `kriteria_gap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nilai`
--
ALTER TABLE `nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `penghasilan`
--
ALTER TABLE `penghasilan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `peringkat_prestasi`
--
ALTER TABLE `peringkat_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `range_bobot`
--
ALTER TABLE `range_bobot`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ranking`
--
ALTER TABLE `ranking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rulebase`
--
ALTER TABLE `rulebase`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `santri`
--
ALTER TABLE `santri`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `selisih_gap`
--
ALTER TABLE `selisih_gap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_lembaga`
--
ALTER TABLE `status_lembaga`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status` (`kode`);

--
-- Indexes for table `status_siswa`
--
ALTER TABLE `status_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `status_wali`
--
ALTER TABLE `status_wali`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tingkat_prestasi`
--
ALTER TABLE `tingkat_prestasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `alat_transportasi`
--
ALTER TABLE `alat_transportasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `asal_sekolah`
--
ALTER TABLE `asal_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `bidang_prestasi`
--
ALTER TABLE `bidang_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `cita_cita`
--
ALTER TABLE `cita_cita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `fuzzyoutput`
--
ALTER TABLE `fuzzyoutput`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `hoby`
--
ALTER TABLE `hoby`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `jarak`
--
ALTER TABLE `jarak`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jenis_pekerjaan`
--
ALTER TABLE `jenis_pekerjaan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `jenis_sekolah`
--
ALTER TABLE `jenis_sekolah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `jenjang_pendidikan`
--
ALTER TABLE `jenjang_pendidikan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `nilai`
--
ALTER TABLE `nilai`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `penghasilan`
--
ALTER TABLE `penghasilan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `peringkat_prestasi`
--
ALTER TABLE `peringkat_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `range_bobot`
--
ALTER TABLE `range_bobot`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `ranking`
--
ALTER TABLE `ranking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `rulebase`
--
ALTER TABLE `rulebase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `santri`
--
ALTER TABLE `santri`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `selisih_gap`
--
ALTER TABLE `selisih_gap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `status_lembaga`
--
ALTER TABLE `status_lembaga`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `status_siswa`
--
ALTER TABLE `status_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `status_wali`
--
ALTER TABLE `status_wali`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tingkat_prestasi`
--
ALTER TABLE `tingkat_prestasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
