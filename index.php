<?php
session_start();
error_reporting(E_ALL && ~E_NOTICE);
use Controller\AlatTransportasiController;
use Controller\AsalSekolahController;
use Controller\AuthController;
use Controller\BidangPrestasiController;
use Controller\CitaCitaController;
use Controller\HobyController;
use Controller\JarakController;
use Controller\JenisPekerjaanController;
use Controller\JenisSekolahController;
use Controller\JenjangPendidikanController;
use Controller\PenghasilanController;
use Controller\PeringkatPrestasiController;
use Controller\SantriController;
use Controller\StatusLembagaController;
use Controller\StatusSiswaController;
use Controller\StatusWaliController;
use Controller\TingkatPrestasiController;
use Controller\UserController;

require "vendor/autoload.php";

$app = new Slim\Slim();
define('URLS', $app->request->getRootUri());
$alattransportasi = new AlatTransportasiController();
$auth = new AuthController();
$asalsekolah = new AsalSekolahController();
$bidangprestasi = new BidangPrestasiController();
$citacita = new CitaCitaController();
$hoby = new HobyController();
$jarak = new JarakController();
$jenispekerjaan = new JenisPekerjaanController();
$jenissekolah = new JenisSekolahController();
$jenjangpendidikan = new JenjangPendidikanController();
$penghasilan = new PenghasilanController();
$peringkatprestasi = new PeringkatPrestasiController();
$statuslembaga = new StatusLembagaController();
$statussiswa = new StatusSiswaController();
$statuswali = new StatusWaliController();
$santri = new SantriController();
$tingkatprestasi = new TingkatPrestasiController();
$user = new UserController();

\Controller\AuthController::authenticate($app->request->getResourceUri());
$app->get('/', function () {

    \Config\View::render('layouts.content');
});
$app->get('/alattransportasi', function () use ($alattransportasi) {
    $alattransportasi->index();
});
$app->get('/alattransportasi/create', function () use ($alattransportasi) {
    $alattransportasi->create();
});
$app->post('/alattransportasi/create', function () use ($app, $alattransportasi) {

    $alattransportasi->store($app->request()->post());
});
$app->get('/alattransportasi/update/:id', function ($id) use ($alattransportasi) {
    $alattransportasi->edit($id);
});
$app->post('/alattransportasi/update/:id', function ($id) use ($app, $alattransportasi) {
    $alattransportasi->update($id, $app->request()->post());
});
$app->get('/alattransportasi/delete/:id', function ($id) use ($app, $alattransportasi) {
    $alattransportasi->delete($id);
});
$app->get('/asalsekolah', function () use ($asalsekolah) {
    $asalsekolah->index();
});
$app->get('/asalsekolah/create', function () use ($asalsekolah) {
    $asalsekolah->create();
});
$app->post('/asalsekolah/create', function () use ($app, $asalsekolah) {
    $asalsekolah->store($app->request()->post());
});
$app->get('/asalsekolah/update/:id', function ($id) use ($asalsekolah) {
    $asalsekolah->edit($id);
});
$app->post('/asalsekolah/update/:id', function ($id) use ($app, $asalsekolah) {
    $asalsekolah->update($id, $app->request()->post());
});
$app->get('/asalsekolah/delete/:id', function ($id) use ($app, $asalsekolah) {
    $asalsekolah->delete($id);
});
$app->get('/bidangprestasi', function () use ($bidangprestasi) {
    $bidangprestasi->index();
});
$app->get('/bidangprestasi/create', function () use ($bidangprestasi) {
    $bidangprestasi->create();
});
$app->post('/bidangprestasi/create', function () use ($app, $bidangprestasi) {
    $bidangprestasi->store($app->request()->post());
});
$app->get('/bidangprestasi/update/:id', function ($id) use ($bidangprestasi) {
    $bidangprestasi->edit($id);
});
$app->post('/bidangprestasi/update/:id', function ($id) use ($app, $bidangprestasi) {
    $bidangprestasi->update($id, $app->request()->post());
});
$app->get('/bidangprestasi/delete/:id', function ($id) use ($app, $bidangprestasi) {
    $bidangprestasi->delete($id);
});
$app->get('/citacita', function () use ($citacita) {
    $citacita->index();
});
$app->get('/citacita/create', function () use ($citacita) {
    $citacita->create();
});
$app->post('/citacita/create', function () use ($app, $citacita) {
    $citacita->store($app->request()->post());
});
$app->get('/citacita/update/:id', function ($id) use ($citacita) {
    $citacita->edit($id);
});
$app->post('/citacita/update/:id', function ($id) use ($app, $citacita) {
    $citacita->update($id, $app->request()->post());
});
$app->get('/citacita/delete/:id', function ($id) use ($app, $citacita) {
    $citacita->delete($id);
});
$app->get('/hoby', function () use ($hoby) {
    $hoby->index();
});
$app->get('/hoby/create', function () use ($hoby) {
    $hoby->create();
});
$app->post('/hoby/create', function () use ($app, $hoby) {
    $hoby->store($app->request()->post());
});
$app->get('/hoby/update/:id', function ($id) use ($hoby) {
    $hoby->edit($id);
});
$app->post('/hoby/update/:id', function ($id) use ($app, $hoby) {
    $hoby->update($id, $app->request()->post());
});
$app->get('/hoby/delete/:id', function ($id) use ($app, $hoby) {
    $hoby->delete($id);
});
$app->get('/jarak', function () use ($jarak) {
    $jarak->index();
});
$app->get('/jarak/create', function () use ($jarak) {
    $jarak->create();
});
$app->post('/jarak/create', function () use ($app, $jarak) {
    $jarak->store($app->request()->post());
});
$app->get('/jarak/update/:id', function ($id) use ($jarak) {
    $jarak->edit($id);
});
$app->post('/jarak/update/:id', function ($id) use ($app, $jarak) {
    $jarak->update($id, $app->request()->post());
});
$app->get('/jarak/delete/:id', function ($id) use ($app, $jarak) {
    $jarak->delete($id);
});
$app->get('/jenispekerjaan', function () use ($jenispekerjaan) {
    $jenispekerjaan->index();
});
$app->get('/jenispekerjaan/create', function () use ($jenispekerjaan) {
    $jenispekerjaan->create();
});
$app->post('/jenispekerjaan/create', function () use ($app, $jenispekerjaan) {
    $jenispekerjaan->store($app->request()->post());
});
$app->get('/jenispekerjaan/update/:id', function ($id) use ($jenispekerjaan) {
    $jenispekerjaan->edit($id);
});
$app->post('/jenispekerjaan/update/:id', function ($id) use ($app, $jenispekerjaan) {
    $jenispekerjaan->update($id, $app->request()->post());
});
$app->get('/jenispekerjaan/delete/:id', function ($id) use ($app, $jenispekerjaan) {
    $jenispekerjaan->delete($id);
});
$app->get('/jenissekolah', function () use ($jenissekolah) {
    $jenissekolah->index();
});
$app->get('/jenissekolah/create', function () use ($jenissekolah) {
    $jenissekolah->create();
});
$app->post('/jenissekolah/create', function () use ($app, $jenissekolah) {
    $jenissekolah->store($app->request()->post());
});
$app->get('/jenissekolah/update/:id', function ($id) use ($jenissekolah) {
    $jenissekolah->edit($id);
});
$app->post('/jenissekolah/update/:id', function ($id) use ($app, $jenissekolah) {
    $jenissekolah->update($id, $app->request()->post());
});
$app->get('/jenissekolah/delete/:id', function ($id) use ($app, $jenissekolah) {
    $jenissekolah->delete($id);
});
$app->get('/jenjangpendidikan', function () use ($jenjangpendidikan) {
    $jenjangpendidikan->index();
});
$app->get('/jenjangpendidikan/create', function () use ($jenjangpendidikan) {
    $jenjangpendidikan->create();
});
$app->post('/jenjangpendidikan/create', function () use ($app, $jenjangpendidikan) {
    $jenjangpendidikan->store($app->request()->post());
});
$app->get('/jenjangpendidikan/update/:id', function ($id) use ($jenjangpendidikan) {
    $jenjangpendidikan->edit($id);
});
$app->post('/jenjangpendidikan/update/:id', function ($id) use ($app, $jenjangpendidikan) {
    $jenjangpendidikan->update($id, $app->request()->post());
});
$app->get('/jenjangpendidikan/delete/:id', function ($id) use ($app, $jenjangpendidikan) {
    $jenjangpendidikan->delete($id);
});
$app->get('/penghasilan', function () use ($penghasilan) {
    $penghasilan->index();
});
$app->get('/penghasilan/create', function () use ($penghasilan) {
    $penghasilan->create();
});
$app->post('/penghasilan/create', function () use ($app, $penghasilan) {
    $penghasilan->store($app->request()->post());
});
$app->get('/penghasilan/update/:id', function ($id) use ($penghasilan) {
    $penghasilan->edit($id);
});
$app->post('/penghasilan/update/:id', function ($id) use ($app, $penghasilan) {
    $penghasilan->update($id, $app->request()->post());
});
$app->get('/penghasilan/delete/:id', function ($id) use ($app, $penghasilan) {
    $penghasilan->delete($id);
});
$app->get('/peringkatprestasi', function () use ($peringkatprestasi) {
    $peringkatprestasi->index();
});
$app->get('/peringkatprestasi/create', function () use ($peringkatprestasi) {
    $peringkatprestasi->create();
});
$app->post('/peringkatprestasi/create', function () use ($app, $peringkatprestasi) {
    $peringkatprestasi->store($app->request()->post());
});
$app->get('/peringkatprestasi/update/:id', function ($id) use ($peringkatprestasi) {
    $peringkatprestasi->edit($id);
});
$app->post('/peringkatprestasi/update/:id', function ($id) use ($app, $peringkatprestasi) {
    $peringkatprestasi->update($id, $app->request()->post());
});
$app->get('/peringkatprestasi/delete/:id', function ($id) use ($app, $peringkatprestasi) {
    $peringkatprestasi->delete($id);
});
$app->get('/statuslembaga', function () use ($statuslembaga) {
    $statuslembaga->index();
});
$app->get('/statuslembaga/create', function () use ($statuslembaga) {
    $statuslembaga->create();
});
$app->post('/statuslembaga/create', function () use ($app, $statuslembaga) {
    $statuslembaga->store($app->request()->post());
});
$app->get('/statuslembaga/update/:id', function ($id) use ($statuslembaga) {
    $statuslembaga->edit($id);
});
$app->post('/statuslembaga/update/:id', function ($id) use ($app, $statuslembaga) {
    $statuslembaga->update($id, $app->request()->post());
});
$app->get('/statuslembaga/delete/:id', function ($id) use ($app, $statuslembaga) {
    $statuslembaga->delete($id);
});
$app->get('/statussiswa', function () use ($statussiswa) {
    $statussiswa->index();
});
$app->get('/statussiswa/create', function () use ($statussiswa) {
    $statussiswa->create();
});
$app->post('/statussiswa/create', function () use ($app, $statussiswa) {
    $statussiswa->store($app->request()->post());
});
$app->get('/statussiswa/update/:id', function ($id) use ($statussiswa) {
    $statussiswa->edit($id);
});
$app->post('/statussiswa/update/:id', function ($id) use ($app, $statussiswa) {
    $statussiswa->update($id, $app->request()->post());
});
$app->get('/statussiswa/delete/:id', function ($id) use ($app, $statussiswa) {
    $statussiswa->delete($id);
});
$app->get('/statuswali', function () use ($statuswali) {
    $statuswali->index();
});
$app->get('/statuswali/create', function () use ($statuswali) {
    $statuswali->create();
});
$app->post('/statuswali/create', function () use ($app, $statuswali) {
    $statuswali->store($app->request()->post());
});
$app->get('/statuswali/update/:id', function ($id) use ($statuswali) {
    $statuswali->edit($id);
});
$app->post('/statuswali/update/:id', function ($id) use ($app, $statuswali) {
    $statuswali->update($id, $app->request()->post());
});
$app->get('/statuswali/delete/:id', function ($id) use ($app, $statuswali) {
    $statuswali->delete($id);
});
$app->get('/tingkatprestasi', function () use ($tingkatprestasi) {
    $tingkatprestasi->index();
});
$app->get('/tingkatprestasi/create', function () use ($tingkatprestasi) {
    $tingkatprestasi->create();
});
$app->post('/tingkatprestasi/create', function () use ($app, $tingkatprestasi) {
    $tingkatprestasi->store($app->request()->post());
});
$app->get('/tingkatprestasi/update/:id', function ($id) use ($tingkatprestasi) {
    $tingkatprestasi->edit($id);
});
$app->post('/tingkatprestasi/update/:id', function ($id) use ($app, $tingkatprestasi) {
    $tingkatprestasi->update($id, $app->request()->post());
});
$app->get('/tingkatprestasi/delete/:id', function ($id) use ($app, $tingkatprestasi) {
    $tingkatprestasi->delete($id);
});
$app->get('/santri', function () use ($santri) {
    $santri->index();
});
$app->get('/santri/listnilai/:id', function ($id) use ($santri) {
    $santri->indexbystatuslembaga($id);
});
$app->get('/santri/excel/:id', function ($id) use ($santri) {
    $santri->excelall($id);
});
$app->get('/santri/statuslembaga/:id', function ($id) use ($santri) {
    $santri->allbystatuslembaga($id);
});
$app->get('/santri/excel/:id', function ($id) use ($santri) {
    include "View/Santri/excel.php";
});
$app->get('/santri/create', function () use ($santri) {
    $santri->create();
});
$app->post('/santri/create', function () use ($app, $santri) {
    $santri->store($app->request()->post());
});
$app->get('/santri/update/:id', function ($id) use ($santri) {
    $santri->edit($id);
});
$app->post('/santri/update/:id', function ($id) use ($app, $santri) {
    $santri->update($id, $app->request()->post());
});
$app->get('/santri/nilai/:id', function ($id) use ($santri) {
    $santri->nilaicreate($id);
});
$app->post('/santri/nilai/:id', function ($id) use ($app, $santri) {
    $santri->updatenilai($id, $app->request()->post());
});
$app->get('/santri/delete/:id', function ($id) use ($app, $santri) {
    $santri->delete($id);
});
$app->get('/user', function () use ($app, $user) {
    $user->index();
});
$app->get('/user/create', function () use ($app, $user) {
    $user->create();
});
$app->post('/user/create', function () use ($app, $user) {
    $user->store($app->request()->post());
});
$app->get('/user/delete/:id', function ($id) use ($app, $user) {
    $user->delete($id);
});
$app->get('/user/update/:id', function ($id) use ($app, $user) {
    $user->edit($id);
});
$app->post('/user/update/:id', function ($id) use ($app, $user) {
    $user->update($id,$app->request()->post());
});
$app->get('/user/updatepass/:id', function ($id) use ($app, $user) {
    $user->editpass($id);
});
$app->post('/user/updatepass/:id', function ($id) use ($app, $user) {
    $user->updatepass($id,$app->request()->post());
});
$app->get('/kriteriagap', function () use ($app, $santri) {
    $santri->editgap();
});
$app->post('/kriteriagap', function () use ($app, $santri) {
    $santri->updategap($app->request()->post());
});
$app->get('/login', function () {
    include "View/layouts/login.php";
});
$app->post('/login', function () use ($app, $auth) {
    $auth->auth($app->request()->post());
});
$app->get('/logout', function () {
    session_destroy();
    echo "<script type='text/javascript'>document.location='" . URLS . "/'</script>";
});
$app->get('/crypt', function () {
    echo $salt = "3" . substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
    echo "<br>" . $hash = crypt('foo', '$2y$12$' . $salt);
    echo "<br>";
    var_dump($hash == crypt('foo', $hash));
    var_dump($hash == crypt('bar', $hash));
});
$app->run();
