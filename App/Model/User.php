<?php



namespace Model;


use Config\Config;
use PDO;
use PDOException;

class User
{
    private $db;

    /**
     * Phasa constructor.
     */
    public function __construct()
    {
        $this->db = Config::getConnection();
    }

    public function all()
    {
        try {
            $query = "SELECT * FROM user WHERE ISNULL(deleted_at)  ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function detail($id)
    {
        try {
            $query = "SELECT * FROM user WHERE ISNULL(deleted_at) and id = $id  ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function delete($id)
    {
        try {
            $sql = "DELETE FROM user  where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);

            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }
    public function insert($data)
    {
        try {
            $salt = "3" . substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash = crypt($data['password'], '$2y$12$' . $salt);
            $sql = "INSERT INTO user VALUES (NULL,:nama,:email,:password,:level,:created_at,NULL,NULL,:created_by,NULL,NULL)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':email', $data['email']);
            $stmt->bindparam(':password', $hash);
            $stmt->bindparam(':level', $data['level']);
            $stmt->bindparam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':created_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }
    public function update($id, $data)
    {
        try {
            $salt = "3" . substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash = crypt($data['password'], '$2y$12$' . $salt);
            $sql = "UPDATE user set nama=:nama,email=:email,level=:level,updated_at=:updated_at,updated_by=:updated_by where id =:id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':email', $data['email']);
            $stmt->bindparam(':level', $data['level']);
            $stmt->bindparam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':updated_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "", "id" => $this->db->lastInsertId());
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }
    public function updatepass($id, $data)
    {
        try {
            $salt = "3" . substr(strtr(base64_encode(openssl_random_pseudo_bytes(22)), '+', '.'), 0, 22);
            $hash = crypt($data['password'], '$2y$12$' . $salt);
            $sql = "UPDATE user set password=:password,updated_at=:updated_at,updated_by=:updated_by where id =:id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':password', $hash);
            $stmt->bindparam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':updated_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "", "id" => $this->db->lastInsertId());
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function auth($data)
    {
        try {
            $query = "SELECT * from user WHERE email=:email";
            $stmt = $this->db->prepare($query);
            $stmt->bindParam(':email', $data['email']);
            $user = array();
            $stmt->execute();
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    if ($row['password'] == crypt($data['password'], $row['password'])) {
                        $user = $row;
                        break;
                    }
                }
                $stmt->closeCursor();
                return array("success" => true, "data" => $user, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }
}