<?php



namespace Model;


use Config\Config;
use PDO;
use PDOException;

class Nilai
{
    private $db;

    /**
     * Phasa constructor.
     */
    public function __construct()
    {
        $this->db = Config::getConnection();
    }

    public function all()
    {
        try {
            $query = "SELECT * FROM santri,nilai WHERE santri.id = nilai.santri_id AND ISNULL(santri.deleted_at)  ORDER by santri.id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function allbystatuslembaga($id)
    {
        try {
            $query = "SELECT *,santri.id as santri_id,fuzzyoutput.fo,ranking.nilai as ranknil FROM nilai,fuzzyoutput,  santri LEFT OUTER JOIN  ranking on santri.id = ranking.santri_id  WHERE santri.id = fuzzyoutput.santri_id  and santri.id = nilai.santri_id and santri.status_lembaga = $id AND ISNULL(santri.deleted_at) GROUP BY santri.id   ORDER by ranknil DESC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function rule()
    {
        try {
            $query = "SELECT * FROM rulebase GROUP BY nilai";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function getnilaigap()
    {
        try {
            $query = "SELECT * FROM kriteria_gap WHERE id = 1";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }


    public function getselisihgap($id)
    {
        try {
            $query = "SELECT * FROM selisih_gap WHERE selisih = '$id'";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function getrangegap($id)
    {
        try {
            $query = "SELECT * FROM range_bobot WHERE  $id BETWEEN range_kecil AND range_besar";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function update($id, $data)
    {
        try {
            $sql = "UPDATE nilai set adzan=:adzan,sholat=:sholat,quran=:quran where santri_id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':adzan', $data['adzan']);
            $stmt->bindparam(':sholat', $data['sholat']);
            $stmt->bindparam(':quran', $data['quran']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function truncatehasil()
    {
        try {
            $sql = "TRUNCATE fuzzyoutput";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function truncateranking()
    {
        try {
            $sql = "TRUNCATE ranking";
            $stmt = $this->db->prepare($sql);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function updatefo($id, $data)
    {
        try {
            $sql = "INSERT INTO fuzzyoutput VALUES(NULL,:id,:fo) ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':fo', $data['fo']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function updaterank($id, $data)
    {
        try {
            $sql = "INSERT INTO ranking VALUES(NULL,:fo,:id) ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':fo', $data);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function updategap($data)
    {
        try {
            $sql = "UPDATE kriteria_gap set adzan=:adzan,sholat=:sholat,quran=:quran where id =1";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':adzan', $data['adzan']);
            $stmt->bindparam(':sholat', $data['sholat']);
            $stmt->bindparam(':quran', $data['quran']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function detail($id)
    {
        try {
            $query = "SELECT * FROM santri,nilai WHERE santri.id = nilai.santri_id AND santri.id = $id AND ISNULL(santri.deleted_at)  ORDER by santri.id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function rulebase($data)
    {
        try {
            $query = "SELECT * FROM rulebase WHERE  adzan=:adzan and sholat=:sholat and quran=:quran";
            $stmt = $this->db->prepare($query);
            $stmt->bindparam(':adzan', $data['adzan']);
            $stmt->bindparam(':sholat', $data['sholat']);
            $stmt->bindparam(':quran', $data['quran']);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }
}