<?php

namespace Model;

use Config\Config;
use PDO;
use PDOException;


class StatusLembaga
{
    private $db;

    /**
     * Phasa constructor.
     */
    public function __construct()
    {
        $this->db = Config::getConnection();
    }

    public function all()
    {
        try {
            $query = "SELECT * FROM status_lembaga WHERE ISNULL(deleted_at)  ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function detail($id)
    {
        try {
            $query = "SELECT * FROM status_lembaga WHERE ISNULL(deleted_at) and id = $id  ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }


    public function insert($data)
    {
        try {
            $sql = "INSERT INTO status_lembaga VALUES (NULL,:nama,:kode,:created_at,NULL,NULL,:created_by,NULL,NULL)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':kode', $data['kode']);
            $stmt->bindparam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':created_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function update($id, $data)
    {
        try {
            $sql = "UPDATE status_lembaga set nama=:nama,kode=:kode,updated_at=:updated_at,updated_by=:updated_by where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':kode', $data['kode']);
            $stmt->bindparam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':updated_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function softdelete($id)
    {
        try {
            $sql = "UPDATE status_lembaga set deleted_at=:deleted_at,deleted_by=:deleted_by where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':deleted_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':deleted_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $sql = "DELETE FROM status_lembaga  where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);

            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

}