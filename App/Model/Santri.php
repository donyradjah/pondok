<?php


namespace Model;


use Config\Config;
use PDO;
use PDOException;

class Santri
{
    private $db;

    /**
     * Phasa constructor.
     */
    public function __construct()
    {
        $this->db = Config::getConnection();
    }

    public function all()
    {
        try {

            $query = "SELECT * FROM santri WHERE ISNULL(deleted_at)  ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function allstatuslembaga($id)
    {
        try {
            $query = "SELECT * FROM santri WHERE ISNULL(deleted_at) and status_lembaga = $id ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = null;
            if ($stmt->columnCount() > 0) {
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                    $array[] = $row;
                }
                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }

    public function insert($data)
    {
        try {
            $query = "SELECT COUNT(*) as NIS FROM santri WHERE ISNULL(deleted_at) and status_lembaga = $data[status_lembaga] ORDER by NIS DESC LIMIT 1";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch();
            $nis = $array['NIS'] + 1;
            $sql = "INSERT INTO santri VALUES (NULL,
                    :status_lembaga,
                    :NIS,
                    :NISN,
                    :NIK,
                    :nama,
                    :tempat_lahir,
                    :tanggal_lahir,
                    :jenis_kelamin,
                    :hoby,
                    :cita_cita,
                    :jumlah_saudara,
                    :status_siswa,
                    :asal_sekolah,
                    :jenis_sekolah,
                    :status_sekolah,
                    :nama_kabupaten_sekolah,
                    :nomor_skhun,
                    :NPSN,
                    :nomor_blanko_skhun,
                    :nomor_ijazah,
                    :total_nilai,
                    :tanggal_lulus,
                    :alamat_ortu,
                    :provinsi_ortu,
                    :nama_kabupaten_ortu,
                    :nama_kecamatan_ortu,
                    :nama_desa_ortu,
                    :kode_pos_ortu,
                    :jarak,
                    :alat_transportasi,
                    :kebutuhan_khusus,
                    :tuna_rungu,
                    :tuna_netra,
                    :tuna_daksa,
                    :tuna_grahita,
                    :tuna_laras,
                    :lamban_belajar,
                    :sulit_belajar,
                    :gangguan_komunikasi,
                    :bakat_luar_biasa,
                    :nomor_kk,
                    :nama_ayah,
                    :NIK_ayah,
                    :jenjang_pendidikan_ayah,
                    :jenis_pekerjaan_ayah,
                    :no_hp_ayah,
                    :nama_ibu,
                    :NIK_ibu,
                    :jenjang_pendidikan_ibu,
                    :jenis_pekerjaan_ibu,
                    :no_hp_ibu,
                    :rata_penghasilan_ortu,
                    :nama_wali,
                    :NIK_wali,
                    :tahun_lahir_wali,
                    :jenjang_pendidikan_wali,
                    :jenis_pekerjaan_wali,
                    :status_wali,
                    :no_hp_wali,
                    :rata_penghasilan_wali,
                    :bidang_prestasi,
                    :tingkat_prestasi,
                    :peringkat_prestasi,
                    :tahun_prestasi,
                    :nomor_kks,
                    :nomor_pkh,
                    :nomor_kip,
                    :created_at,NULL,NULL,:created_by,NULL,NULL,
                    :tahunmasuk)";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':status_lembaga', $data['status_lembaga']);
            $stmt->bindparam(':NIS', $nis);
            $stmt->bindparam(':NISN', $data['NISN']);
            $stmt->bindparam(':NIK', $data['NIK']);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':tempat_lahir', $data['tempat_lahir']);
            $stmt->bindparam(':tanggal_lahir', $data['tanggal_lahir']);
            $stmt->bindparam(':jenis_kelamin', $data['jenis_kelamin']);
            $stmt->bindparam(':hoby', $data['hoby']);
            $stmt->bindparam(':cita_cita', $data['cita_cita']);
            $stmt->bindparam(':jumlah_saudara', $data['jumlah_saudara']);
            $stmt->bindparam(':status_siswa', $data['status_siswa']);
            $stmt->bindparam(':asal_sekolah', $data['asal_sekolah']);
            $stmt->bindparam(':jenis_sekolah', $data['jenis_sekolah']);
            $stmt->bindparam(':status_sekolah', $data['status_sekolah']);
            $stmt->bindparam(':nama_kabupaten_sekolah', $data['nama_kabupaten_sekolah']);
            $stmt->bindparam(':nomor_skhun', $data['nomor_skhun']);
            $stmt->bindparam(':NPSN', $data['NPSN']);
            $stmt->bindparam(':nomor_blanko_skhun', $data['nomor_blanko_skhun']);
            $stmt->bindparam(':nomor_ijazah', $data['nomor_ijazah']);
            $stmt->bindparam(':total_nilai', $data['total_nilai']);
            $stmt->bindparam(':tanggal_lulus', $data['tanggal_lulus']);
            $stmt->bindparam(':alamat_ortu', $data['alamat_ortu']);
            $stmt->bindparam(':provinsi_ortu', $data['provinsi_ortu']);
            $stmt->bindparam(':nama_kabupaten_ortu', $data['nama_kabupaten_ortu']);
            $stmt->bindparam(':nama_kecamatan_ortu', $data['nama_kecamatan_ortu']);
            $stmt->bindparam(':nama_desa_ortu', $data['nama_desa_ortu']);
            $stmt->bindparam(':kode_pos_ortu', $data['kode_pos_ortu']);
            $stmt->bindparam(':jarak', $data['jarak']);
            $stmt->bindparam(':alat_transportasi', $data['alat_transportasi']);
            $stmt->bindparam(':kebutuhan_khusus', $data['kebutuhan_khusus']);
            $stmt->bindparam(':tuna_rungu', $data['tuna_rungu']);
            $stmt->bindparam(':tuna_netra', $data['tuna_netra']);
            $stmt->bindparam(':tuna_daksa', $data['tuna_daksa']);
            $stmt->bindparam(':tuna_grahita', $data['tuna_grahita']);
            $stmt->bindparam(':tuna_laras', $data['tuna_laras']);
            $stmt->bindparam(':lamban_belajar', $data['lamban_belajar']);
            $stmt->bindparam(':sulit_belajar', $data['sulit_belajar']);
            $stmt->bindparam(':gangguan_komunikasi', $data['gangguan_komunikasi']);
            $stmt->bindparam(':bakat_luar_biasa', $data['bakat_luar_biasa']);
            $stmt->bindparam(':nomor_kk', $data['nomor_kk']);
            $stmt->bindparam(':nama_ayah', $data['nama_ayah']);
            $stmt->bindparam(':NIK_ayah', $data['NIK_ayah']);
            $stmt->bindparam(':jenjang_pendidikan_ayah', $data['jenjang_pendidikan_ayah']);
            $stmt->bindparam(':jenis_pekerjaan_ayah', $data['jenis_pekerjaan_ayah']);
            $stmt->bindparam(':no_hp_ayah', $data['no_hp_ayah']);
            $stmt->bindparam(':nama_ibu', $data['nama_ibu']);
            $stmt->bindparam(':NIK_ibu', $data['NIK_ibu']);
            $stmt->bindparam(':jenjang_pendidikan_ibu', $data['jenjang_pendidikan_ibu']);
            $stmt->bindparam(':jenis_pekerjaan_ibu', $data['jenis_pekerjaan_ibu']);
            $stmt->bindparam(':no_hp_ibu', $data['no_hp_ibu']);
            $stmt->bindparam(':rata_penghasilan_ortu', $data['rata_penghasilan_ortu']);
            $stmt->bindparam(':nama_wali', $data['nama_wali']);
            $stmt->bindparam(':NIK_wali', $data['NIK_wali']);
            $stmt->bindparam(':tahun_lahir_wali', $data['tahun_lahir_wali']);
            $stmt->bindparam(':jenjang_pendidikan_wali', $data['jenjang_pendidikan_wali']);
            $stmt->bindparam(':jenis_pekerjaan_wali', $data['jenis_pekerjaan_wali']);
            $stmt->bindparam(':status_wali', $data['status_wali']);
            $stmt->bindparam(':no_hp_wali', $data['no_hp_wali']);
            $stmt->bindparam(':rata_penghasilan_wali', $data['rata_penghasilan_wali']);
            $stmt->bindparam(':bidang_prestasi', $data['bidang_prestasi']);
            $stmt->bindparam(':tingkat_prestasi', $data['tingkat_prestasi']);
            $stmt->bindparam(':peringkat_prestasi', $data['peringkat_prestasi']);
            $stmt->bindparam(':tahun_prestasi', $data['tahun_prestasi']);
            $stmt->bindparam(':nomor_kks', $data['nomor_kks']);
            $stmt->bindparam(':nomor_pkh', $data['nomor_pkh']);
            $stmt->bindparam(':nomor_kip', $data['nomor_kip']);
            $stmt->bindparam(':tahunmasuk', date('y'));
            $stmt->bindparam(':created_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':created_by', $_SESSION['id']);
            $stmt->execute();

            $query = "INSERT INTO nilai VALUES (NULL," . $this->db->lastInsertId() . ",0,0,0)";
            $stmt2 = $this->db->prepare($query);
            $stmt2->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function update($id, $data)
    {
        try {

            $sql = "UPDATE santri set 
                    status_lembaga=:status_lembaga,
                    NISN=:NISN,
                    NIK=:NIK,
                    nama=:nama,
                    tempat_lahir=:tempat_lahir,
                    tanggal_lahir=:tanggal_lahir,
                    jenis_kelamin=:jenis_kelamin,
                    hoby=:hoby,
                    cita_cita=:cita_cita,
                    jumlah_saudara=:jumlah_saudara,
                    status_siswa=:status_siswa,
                    asal_sekolah=:asal_sekolah,
                    jenis_sekolah=:jenis_sekolah,
                    status_sekolah=:status_sekolah,
                    nama_kabupaten_sekolah=:nama_kabupaten_sekolah,
                    nomor_skhun=:nomor_skhun,
                    NPSN=:NPSN,
                    nomor_blanko_skhun=:nomor_blanko_skhun,
                    nomor_ijazah=:nomor_ijazah,
                    total_nilai=:total_nilai,
                    tanggal_lulus=:tanggal_lulus,
                    alamat_ortu=:alamat_ortu,
                    provinsi_ortu=:provinsi_ortu,
                    nama_kabupaten_ortu=:nama_kabupaten_ortu,
                    nama_kecamatan_ortu=:nama_kecamatan_ortu,
                    nama_desa_ortu=:nama_desa_ortu,
                    kode_pos_ortu=:kode_pos_ortu,
                    jarak=:jarak,
                    alat_transportasi=:alat_transportasi,
                    kebutuhan_khusus=:kebutuhan_khusus,
                    tuna_rungu=:tuna_rungu,
                    tuna_netra=:tuna_netra,
                    tuna_daksa=:tuna_daksa,
                    tuna_grahita=:tuna_grahita,
                    tuna_laras=:tuna_laras,
                    lamban_belajar=:lamban_belajar,
                    sulit_belajar=:sulit_belajar,
                    gangguan_komunikasi=:gangguan_komunikasi,
                    bakat_luar_biasa=:bakat_luar_biasa,
                    nomor_kk=:nomor_kk,
                    nama_ayah=:nama_ayah,
                    NIK_ayah=:NIK_ayah,
                    jenjang_pendidikan_ayah=:jenjang_pendidikan_ayah,
                    jenis_pekerjaan_ayah=:jenis_pekerjaan_ayah,
                    no_hp_ayah=:no_hp_ayah,
                    nama_ibu=:nama_ibu,
                    NIK_ibu=:NIK_ibu,
                    jenjang_pendidikan_ibu=:jenjang_pendidikan_ibu,
                    jenis_pekerjaan_ibu=:jenis_pekerjaan_ibu,
                    no_hp_ibu=:no_hp_ibu,
                    rata_penghasilan_ortu=:rata_penghasilan_ortu,
                    nama_wali=:nama_wali,
                    NIK_wali=:NIK_wali,
                    tahun_lahir_wali=:tahun_lahir_wali,
                    jenjang_pendidikan_wali=:jenjang_pendidikan_wali,
                    jenis_pekerjaan_wali=:jenis_pekerjaan_wali,
                    status_wali=:status_wali,
                    no_hp_wali=:no_hp_wali,
                    rata_penghasilan_wali=:rata_penghasilan_wali,
                    bidang_prestasi=:bidang_prestasi,
                    tingkat_prestasi=:tingkat_prestasi,
                    peringkat_prestasi=:peringkat_prestasi,
                    tahun_prestasi=:tahun_prestasi,
                    nomor_kks=:nomor_kks,
                    nomor_pkh=:nomor_pkh,
                    nomor_kip=:nomor_kip,
                    tahunmasuk=:tahunmasuk,
                    updated_at=:updated_at,
                    updated_by=:updated_by 
                    WHERE id =:id";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':status_lembaga', $data['status_lembaga']);
            $stmt->bindparam(':NISN', $data['NISN']);
            $stmt->bindparam(':NIK', $data['NIK']);
            $stmt->bindparam(':nama', $data['nama']);
            $stmt->bindparam(':tempat_lahir', $data['tempat_lahir']);
            $stmt->bindparam(':tanggal_lahir', $data['tanggal_lahir']);
            $stmt->bindparam(':jenis_kelamin', $data['jenis_kelamin']);
            $stmt->bindparam(':hoby', $data['hoby']);
            $stmt->bindparam(':cita_cita', $data['cita_cita']);
            $stmt->bindparam(':jumlah_saudara', $data['jumlah_saudara']);
            $stmt->bindparam(':status_siswa', $data['status_siswa']);
            $stmt->bindparam(':asal_sekolah', $data['asal_sekolah']);
            $stmt->bindparam(':jenis_sekolah', $data['jenis_sekolah']);
            $stmt->bindparam(':status_sekolah', $data['status_sekolah']);
            $stmt->bindparam(':nama_kabupaten_sekolah', $data['nama_kabupaten_sekolah']);
            $stmt->bindparam(':nomor_skhun', $data['nomor_skhun']);
            $stmt->bindparam(':NPSN', $data['NPSN']);
            $stmt->bindparam(':nomor_blanko_skhun', $data['nomor_blanko_skhun']);
            $stmt->bindparam(':nomor_ijazah', $data['nomor_ijazah']);
            $stmt->bindparam(':total_nilai', $data['total_nilai']);
            $stmt->bindparam(':tanggal_lulus', $data['tanggal_lulus']);
            $stmt->bindparam(':alamat_ortu', $data['alamat_ortu']);
            $stmt->bindparam(':provinsi_ortu', $data['provinsi_ortu']);
            $stmt->bindparam(':nama_kabupaten_ortu', $data['nama_kabupaten_ortu']);
            $stmt->bindparam(':nama_kecamatan_ortu', $data['nama_kecamatan_ortu']);
            $stmt->bindparam(':nama_desa_ortu', $data['nama_desa_ortu']);
            $stmt->bindparam(':kode_pos_ortu', $data['kode_pos_ortu']);
            $stmt->bindparam(':jarak', $data['jarak']);
            $stmt->bindparam(':alat_transportasi', $data['alat_transportasi']);
            $stmt->bindparam(':kebutuhan_khusus', $data['kebutuhan_khusus']);
            $stmt->bindparam(':tuna_rungu', $data['tuna_rungu']);
            $stmt->bindparam(':tuna_netra', $data['tuna_netra']);
            $stmt->bindparam(':tuna_daksa', $data['tuna_daksa']);
            $stmt->bindparam(':tuna_grahita', $data['tuna_grahita']);
            $stmt->bindparam(':tuna_laras', $data['tuna_laras']);
            $stmt->bindparam(':lamban_belajar', $data['lamban_belajar']);
            $stmt->bindparam(':sulit_belajar', $data['sulit_belajar']);
            $stmt->bindparam(':gangguan_komunikasi', $data['gangguan_komunikasi']);
            $stmt->bindparam(':bakat_luar_biasa', $data['bakat_luar_biasa']);
            $stmt->bindparam(':nomor_kk', $data['nomor_kk']);
            $stmt->bindparam(':nama_ayah', $data['nama_ayah']);
            $stmt->bindparam(':NIK_ayah', $data['NIK_ayah']);
            $stmt->bindparam(':jenjang_pendidikan_ayah', $data['jenjang_pendidikan_ayah']);
            $stmt->bindparam(':jenis_pekerjaan_ayah', $data['jenis_pekerjaan_ayah']);
            $stmt->bindparam(':no_hp_ayah', $data['no_hp_ayah']);
            $stmt->bindparam(':nama_ibu', $data['nama_ibu']);
            $stmt->bindparam(':NIK_ibu', $data['NIK_ibu']);
            $stmt->bindparam(':jenjang_pendidikan_ibu', $data['jenjang_pendidikan_ibu']);
            $stmt->bindparam(':jenis_pekerjaan_ibu', $data['jenis_pekerjaan_ibu']);
            $stmt->bindparam(':no_hp_ibu', $data['no_hp_ibu']);
            $stmt->bindparam(':rata_penghasilan_ortu', $data['rata_penghasilan_ortu']);
            $stmt->bindparam(':nama_wali', $data['nama_wali']);
            $stmt->bindparam(':NIK_wali', $data['NIK_wali']);
            $stmt->bindparam(':tahun_lahir_wali', $data['tahun_lahir_wali']);
            $stmt->bindparam(':jenjang_pendidikan_wali', $data['jenjang_pendidikan_wali']);
            $stmt->bindparam(':jenis_pekerjaan_wali', $data['jenis_pekerjaan_wali']);
            $stmt->bindparam(':status_wali', $data['status_wali']);
            $stmt->bindparam(':no_hp_wali', $data['no_hp_wali']);
            $stmt->bindparam(':rata_penghasilan_wali', $data['rata_penghasilan_wali']);
            $stmt->bindparam(':bidang_prestasi', $data['bidang_prestasi']);
            $stmt->bindparam(':tingkat_prestasi', $data['tingkat_prestasi']);
            $stmt->bindparam(':peringkat_prestasi', $data['peringkat_prestasi']);
            $stmt->bindparam(':tahun_prestasi', $data['tahun_prestasi']);
            $stmt->bindparam(':nomor_kks', $data['nomor_kks']);
            $stmt->bindparam(':nomor_pkh', $data['nomor_pkh']);
            $stmt->bindparam(':nomor_kip', $data['nomor_kip']);
            $stmt->bindparam(':tahunmasuk', date('y'));
            $stmt->bindparam(':updated_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':updated_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function softdelete($id)
    {
        try {
            $sql = "UPDATE santri set deleted_at=:deleted_at,deleted_by=:deleted_by where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);
            $stmt->bindparam(':deleted_at', date('Y-m-d H:i:s'));
            $stmt->bindparam(':deleted_by', $_SESSION['id']);
            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $data = $this->detail($id)['data'];
            $sql = "DELETE FROM santri  where id =:id ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $id);

            $stmt->execute();

            $sql = "UPDATE santri set NIS = (NIS - 1) where NIS >:id and status_lembaga =:status_lembaga ";
            $stmt = $this->db->prepare($sql);
            $stmt->bindparam(':id', $data['NIS']);
            $stmt->bindparam(':status_lembaga', $data['status_lembaga']);

            $stmt->execute();

            return array("success" => true, "message" => "");
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e->getMessage());
        }
    }

    public function detail($id)
    {
        try {
            $query = "SELECT * FROM santri WHERE ISNULL(deleted_at) and id = $id  ORDER by id ASC";
            $stmt = $this->db->prepare($query);
            $stmt->execute();
            $array = $stmt->fetch(PDO::FETCH_ASSOC);

            if ($array > 0) {

                $stmt->closeCursor();

                return array("success" => true, "data" => $array, "message" => null);
            } else {
                $stmt->closeCursor();

                return array("success" => true, "data" => null, "message" => null);
            }
        } catch (PDOException $e) {
            return array("success" => false, "message" => $e);
        }
    }
}