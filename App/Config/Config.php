<?php



namespace Config;


use PDO;
use PDOException;

class Config
{
    public static function getConnection($host = "localhost", $db_name = "pondok_baru", $username = "root", $password = "")
    {
        $conn = null;

        try {
            $conn = new PDO("mysql:host=" . $host . ";dbname=" . $db_name, $username, $password);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $exception) {
            echo "Connection error: " . $exception->getMessage();
        }

        return $conn;
    }

    public static function error_message_data_master()
    {
        $data = [
            'notEmpty' => ' Input Tidak Boleh Kosong.',
            'stringType' => ' Input harus string.',
            'length' => ' Banyak karakter di input harus ada di antara {{minValue}} dan {{maxValue}} .',
        ];

        return $data;
    }

    public static function error_message_data_status_lembaga()
    {
        $data = [
            'notEmpty' => ' - {{name}} -  Input Tidak Boleh Kosong.',
            'stringType' => ' - {{name}} -  Input harus string.',
            'length' => ' - {{name}} -  Banyak karakter di input harus ada di antara {{minValue}} dan {{maxValue}} .',
        ];

        return $data;
    }

    public static function error_message_data_user()
    {
        $data = [
            'notEmpty' => ' - {{name}} -  Input Tidak Boleh Kosong.',
            'equals' => ' - Password dan Konfirmasi password harus sama',
        ];

        return $data;
    }
}