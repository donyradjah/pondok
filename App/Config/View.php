<?php



namespace Config;


class View
{
    public static function render($file, $variables = array())
    {

        extract($variables);
        $file = explode(".", $file);

        include "View/layouts/header.php";
        include "View/layouts/sidebar.php";
        include "View/" . implode("/", $file) . ".php";
        include "View/layouts/footer.php";
    }


}