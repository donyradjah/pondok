<?php

namespace Controller;

use Config\Config;
use Model\BidangPrestasi;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class BidangPrestasiController
{
    private $bidangprestasi;

    /**
     * BidangPrestasiController constructor.
     * @param $bidangprestasi
     */
    public function __construct()
    {
        $this->bidangprestasi = new BidangPrestasi();
    }

    public function index()
    {
        $bidangprestasi = $this->bidangprestasi->all();
        $data = [
            'success' => $bidangprestasi['success'],
            "data"    => $bidangprestasi['data'],
            "message" => $bidangprestasi['message'],

        ];
        \Config\View::render('BidangPrestasi.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('BidangPrestasi.create', $data);
    }

    public function edit($id, $data = array())
    {
        $bidangprestasi = $this->bidangprestasi->detail($id);
        if (count($bidangprestasi['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($bidangprestasi['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $bidangprestasi['message'],
                    "data"    => $bidangprestasi['data'],
                ];
            } else {
                $arr = [
                    "success" => ($bidangprestasi['success']),
                    "message" => $bidangprestasi['message'],
                    "data"    => $bidangprestasi['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('BidangPrestasi.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Bidang Prestasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->bidangprestasi->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/bidangprestasi'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Bidang Prestasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->bidangprestasi->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/bidangprestasi'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->bidangprestasi->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/bidangprestasi'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/bidangprestasi'</script>";
        }
    }
}