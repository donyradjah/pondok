<?php

namespace Controller;

use Config\Config;
use Model\JenisSekolah;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;



class JenisSekolahController
{
    private $jenis_sekolah;

    /**
     * JenisSekolahController constructor.
     * @param $jenis_sekolah
     */
    public function __construct()
    {
        $this->jenis_sekolah = new JenisSekolah();
    }

    public function index()
    {
        $jenis_sekolah = $this->jenis_sekolah->all();
        $data = [
            'success' => $jenis_sekolah['success'],
            "data"    => $jenis_sekolah['data'],
            "message" => $jenis_sekolah['message'],

        ];
        \Config\View::render('JenisSekolah.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('JenisSekolah.create', $data);
    }

    public function edit($id, $data = array())
    {
        $jenis_sekolah = $this->jenis_sekolah->detail($id);
        if (count($jenis_sekolah['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($jenis_sekolah['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $jenis_sekolah['message'],
                    "data"    => $jenis_sekolah['data'],
                ];
            } else {
                $arr = [
                    "success" => ($jenis_sekolah['success']),
                    "message" => $jenis_sekolah['message'],
                    "data"    => $jenis_sekolah['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('JenisSekolah.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> JenisSekolah" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->jenis_sekolah->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jenissekolah'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> JenisSekolah" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->jenis_sekolah->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jenissekolah'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->jenis_sekolah->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/jenissekolah'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/jenissekolah'</script>";
        }
    }
}