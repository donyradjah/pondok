<?php


namespace Controller;


use Config\Config;
use Model\User;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;

class UserController
{
    private $user;

    /**
     * UserController constructor.
     * @param $user
     */
    public function __construct()
    {
        $this->user = new User();
    }

    public function delete($id)
    {
        $delete = $this->user->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/user'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/user'</script>";
        }
    }
    public function index()
    {
        $user = $this->user->all();
        $data = [
            'success' => $user['success'],
            "data"    => $user['data'],
            "message" => $user['message'],

        ];
        \Config\View::render('User.list', $data);
    }


    public function update($id, $data = array())
    {
        $insert = $this->user->update($id, $data);
        if ($insert['success'] == false) {
            $this->edit($insert);
        } else {
            if ($_SESSION['level'] < 200) {
                $_SESSION['login'] = true;
                $_SESSION['nama'] = $data['nama'];
                $_SESSION['email'] = $data['email'];
                $_SESSION['level'] = $data['level'];
            } elseif ($_SESSION['id'] == $id) {
                $_SESSION['nama'] = $data['nama'];
                $_SESSION['email'] = $data['email'];
                $_SESSION['level'] = $data['level'];
            }
            echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/user'</script>";
        }
    }
    public function updatepass($id, $data = array())
    {
        $insert = $this->user->updatepass($id, $data);
        if ($insert['success'] == false) {
            $this->edit($insert);
        } else {
            echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/user'</script>";
        }
    }

    public function edit($id, $data = array())
    {
        $hoby = $this->user->detail($id);
        if (count($hoby['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($hoby['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $hoby['message'],
                    "data"    => $hoby['data'],
                ];
            } else {
                $arr = [
                    "success" => ($hoby['success']),
                    "message" => $hoby['message'],
                    "data"    => $hoby['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('User.edit', $arr);
    }
    public function editpass($id, $data = array())
    {
        $hoby = $this->user->detail($id);
        if (count($hoby['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($hoby['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $hoby['message'],
                    "data"    => $hoby['data'],
                ];
            } else {
                $arr = [
                    "success" => ($hoby['success']),
                    "message" => $hoby['message'],
                    "data"    => $hoby['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('User.editpassword', $arr);
    }

    public function store($data = array())
    {
        $validator = validator::key('nama', validator::notEmpty())
            ->key('email', validator::notEmpty())
            ->key('password', validator::notEmpty())
            ->key('confirm_password', validator::notEmpty())
            ->key('password', validator::equals($data['confirm_password']));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_user()) as $r) {
                if ($r != "") {
                    $message .= "<li> User" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->user->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/user'</script>";
            }
        }
    }

    public function create($data = array())
    {
        \Config\View::render('User.create', $data);
    }

}