<?php

namespace Controller;

use Config\Config;
use Model\Hoby;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;



class HobyController
{
    private $hoby;

    /**
     * HobyController constructor.
     * @param $hoby
     */
    public function __construct()
    {
        $this->hoby = new Hoby();
    }

    public function index()
    {
        $hoby = $this->hoby->all();
        $data = [
            'success' => $hoby['success'],
            "data"    => $hoby['data'],
            "message" => $hoby['message'],

        ];
        \Config\View::render('Hoby.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('Hoby.create', $data);
    }

    public function edit($id, $data = array())
    {
        $hoby = $this->hoby->detail($id);
        if (count($hoby['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($hoby['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $hoby['message'],
                    "data"    => $hoby['data'],
                ];
            } else {
                $arr = [
                    "success" => ($hoby['success']),
                    "message" => $hoby['message'],
                    "data"    => $hoby['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('Hoby.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Hoby" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->hoby->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/hoby'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Hoby" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->hoby->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/hoby'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->hoby->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/hoby'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/hoby'</script>";
        }
    }
}