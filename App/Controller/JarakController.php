<?php

namespace Controller;

use Config\Config;
use Model\Jarak;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;



class JarakController
{
    private $jarak;

    /**
     * JarakController constructor.
     * @param $jarak
     */
    public function __construct()
    {
        $this->jarak = new Jarak();
    }

    public function index()
    {
        $jarak = $this->jarak->all();
        $data = [
            'success' => $jarak['success'],
            "data"    => $jarak['data'],
            "message" => $jarak['message'],

        ];
        \Config\View::render('Jarak.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('Jarak.create', $data);
    }

    public function edit($id, $data = array())
    {
        $jarak = $this->jarak->detail($id);
        if (count($jarak['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($jarak['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $jarak['message'],
                    "data"    => $jarak['data'],
                ];
            } else {
                $arr = [
                    "success" => ($jarak['success']),
                    "message" => $jarak['message'],
                    "data"    => $jarak['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('Jarak.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Jarak" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->jarak->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jarak'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Jarak" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->jarak->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jarak'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->jarak->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/jarak'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/jarak'</script>";
        }
    }
}