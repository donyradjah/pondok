<?php

namespace Controller;

use Config\Config;
use Model\Penghasilan;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class PenghasilanController
{
    private $penghasilan;

    /**
     * PenghasilanController constructor.
     * @param $penghasilan
     */
    public function __construct()
    {
        $this->penghasilan = new Penghasilan();
    }

    public function index()
    {
        $penghasilan = $this->penghasilan->all();
        $data = [
            'success' => $penghasilan['success'],
            "data"    => $penghasilan['data'],
            "message" => $penghasilan['message'],

        ];
        \Config\View::render('Penghasilan.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('Penghasilan.create', $data);
    }

    public function edit($id, $data = array())
    {
        $penghasilan = $this->penghasilan->detail($id);
        if (count($penghasilan['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($penghasilan['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $penghasilan['message'],
                    "data"    => $penghasilan['data'],
                ];
            } else {
                $arr = [
                    "success" => ($penghasilan['success']),
                    "message" => $penghasilan['message'],
                    "data"    => $penghasilan['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('Penghasilan.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Penghasilan" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->penghasilan->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/penghasilan'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Penghasilan" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->penghasilan->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/penghasilan'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->penghasilan->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/penghasilan'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/penghasilan'</script>";
        }
    }
}