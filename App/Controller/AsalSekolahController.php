<?php

namespace Controller;

use Config\Config;
use Model\AsalSekolah;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class AsalSekolahController
{
    private $asalsekolah;

    /**
     * AsalSekolahController constructor.
     * @param $asalsekolah
     */
    public function __construct()
    {
        $this->asalsekolah = new AsalSekolah();
    }

    public function index()
    {
        $asalsekolah = $this->asalsekolah->all();
        $data = [
            'success' => $asalsekolah['success'],
            "data"    => $asalsekolah['data'],
            "message" => $asalsekolah['message'],

        ];
        \Config\View::render('AsalSekolah.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('AsalSekolah.create', $data);
    }

    public function edit($id, $data = array())
    {
        $asalsekolah = $this->asalsekolah->detail($id);
        if (count($asalsekolah['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($asalsekolah['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $asalsekolah['message'],
                    "data"    => $asalsekolah['data'],
                ];
            } else {
                $arr = [
                    "success" => ($asalsekolah['success']),
                    "message" => $asalsekolah['message'],
                    "data"    => $asalsekolah['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('AsalSekolah.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Asal Sekolah" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->asalsekolah->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/asalsekolah'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Asal Sekolah" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->asalsekolah->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/asalsekolah'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->asalsekolah->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/asalsekolah'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/asalsekolah'</script>";
        }
    }
}