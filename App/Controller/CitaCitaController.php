<?php

namespace Controller;

use Config\Config;
use Model\BidangPrestasi;
use Model\CitaCita;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;



class CitaCitaController
{
    private $citacita;

    /**
     * CitaCitaController constructor.
     * @param $citacita
     */
    public function __construct()
    {
        $this->citacita = new CitaCita();
    }

    public function index()
    {
        $citacita = $this->citacita->all();
        $data = [
            'success' => $citacita['success'],
            "data"    => $citacita['data'],
            "message" => $citacita['message'],

        ];
        \Config\View::render('CitaCita.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('CitaCita.create', $data);
    }

    public function edit($id, $data = array())
    {
        $citacita = $this->citacita->detail($id);
        if (count($citacita['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($citacita['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $citacita['message'],
                    "data"    => $citacita['data'],
                ];
            } else {
                $arr = [
                    "success" => ($citacita['success']),
                    "message" => $citacita['message'],
                    "data"    => $citacita['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('CitaCita.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Cita Cita" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->citacita->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/citacita'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Cita Cita" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->citacita->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/citacita'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->citacita->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/citacita'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/citacita'</script>";
        }
    }
}