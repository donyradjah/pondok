<?php

namespace Controller;

use Config\Config;
use Model\StatusWali;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class StatusWaliController
{
    private $statuswali;

    /**
     * StatusWaliController constructor.
     * @param $statuswali
     */
    public function __construct()
    {
        $this->statuswali = new StatusWali();
    }

    public function index()
    {
        $statuswali = $this->statuswali->all();
        $data = [
            'success' => $statuswali['success'],
            "data"    => $statuswali['data'],
            "message" => $statuswali['message'],

        ];
        \Config\View::render('StatusWali.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('StatusWali.create', $data);
    }

    public function edit($id, $data = array())
    {
        $statuswali = $this->statuswali->detail($id);
        if (count($statuswali['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($statuswali['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $statuswali['message'],
                    "data"    => $statuswali['data'],
                ];
            } else {
                $arr = [
                    "success" => ($statuswali['success']),
                    "message" => $statuswali['message'],
                    "data"    => $statuswali['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('StatusWali.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> StatusWali" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->statuswali->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/statuswali'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> StatusWali" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->statuswali->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/statuswali'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->statuswali->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/statuswali'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/statuswali'</script>";
        }
    }
}