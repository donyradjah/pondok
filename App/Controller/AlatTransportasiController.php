<?php

namespace Controller;

use Config\Config;
use Model\AlatTransportasi;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class AlatTransportasiController
{
    private $alattransportasi;

    /**
     * AlatTransportasiController constructor.
     * @param $alattransportasi
     */
    public function __construct()
    {
        $this->alattransportasi = new AlatTransportasi();

    }

    public function index()
    {
        $alattransportasi = $this->alattransportasi->all();
        $data = [
            'success' => $alattransportasi['success'],
            "data"    => $alattransportasi['data'],
            "message" => $alattransportasi['message'],

        ];
        \Config\View::render('AlatTransportasi.list', $data);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Alat Transportasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->alattransportasi->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/alattransportasi'</script>";
            }
        }
    }

    public function create($data = array())
    {
        \Config\View::render('AlatTransportasi.create', $data);
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Alat Transportasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->alattransportasi->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/alattransportasi'</script>";
            }
        }
    }

    public function edit($id, $data = array())
    {
        $alattransportasi = $this->alattransportasi->detail($id);
        if (count($alattransportasi['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($alattransportasi['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $alattransportasi['message'],
                    "data"    => $alattransportasi['data'],
                ];
            } else {
                $arr = [
                    "success" => ($alattransportasi['success']),
                    "message" => $alattransportasi['message'],
                    "data"    => $alattransportasi['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('AlatTransportasi.edit', $arr);
    }

    public function delete($id)
    {
        $delete = $this->alattransportasi->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/alattransportasi'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/alattransportasi'</script>";
        }
    }
}