<?php


namespace Controller;

use Model\User;

class AuthController
{
    private $user;

    /**
     * UserController constructor.
     * @param $user
     */
    public function __construct()
    {
        $this->user = new User();
    }

    public static function authenticate($url)
    {
        session_start();
        if ($url != "/login") {
            if (!isset($_SESSION['login']) || $_SESSION['login'] == false) {
                echo "<script type='text/javascript'>alert('anda belum login');document.location='" . URLS . "/login'</script>";
            }
        }
    }

    public function auth($data)
    {
        session_start();
        $user = $this->user->auth($data);
        if ((count($user['data']) > 0)) {
            $_SESSION['login'] = true;
            $_SESSION['id'] = $user['data']['id'];
            $_SESSION['nama'] = $user['data']['nama'];
            $_SESSION['email'] = $user['data']['email'];
            $_SESSION['level'] = $user['data']['level'];

            echo "<script type='text/javascript'>document.location='" . URLS . "/'</script>";

        } else {
            $_SESSION['login'] = false;
            echo "<script type='text/javascript'>alert('data tidak di temukan');document.location='" . URLS . "/login'</script>";
        }
    }
}