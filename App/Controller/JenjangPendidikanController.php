<?php

namespace Controller;

use Config\Config;
use Model\JenjangPendidikan;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class JenjangPendidikanController
{
    private $jenjang_pendidikan;

    /**
     * JenjangPendidikanController constructor.
     * @param $jenjang_pendidikan
     */
    public function __construct()
    {
        $this->jenjang_pendidikan = new JenjangPendidikan();
    }

    public function index()
    {
        $jenjang_pendidikan = $this->jenjang_pendidikan->all();
        $data = [
            'success' => $jenjang_pendidikan['success'],
            "data"    => $jenjang_pendidikan['data'],
            "message" => $jenjang_pendidikan['message'],

        ];
        \Config\View::render('JenjangPendidikan.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('JenjangPendidikan.create', $data);
    }

    public function edit($id, $data = array())
    {
        $jenjang_pendidikan = $this->jenjang_pendidikan->detail($id);
        if (count($jenjang_pendidikan['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($jenjang_pendidikan['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $jenjang_pendidikan['message'],
                    "data"    => $jenjang_pendidikan['data'],
                ];
            } else {
                $arr = [
                    "success" => ($jenjang_pendidikan['success']),
                    "message" => $jenjang_pendidikan['message'],
                    "data"    => $jenjang_pendidikan['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('JenjangPendidikan.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> JenjangPendidikan" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->jenjang_pendidikan->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jenjangpendidikan'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> JenjangPendidikan" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->jenjang_pendidikan->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jenjangpendidikan'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->jenjang_pendidikan->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/jenjangpendidikan'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/jenjangpendidikan'</script>";
        }
    }
}