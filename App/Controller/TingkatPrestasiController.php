<?php

namespace Controller;

use Config\Config;
use Model\TingkatPrestasi;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class TingkatPrestasiController
{
    private $tingkatprestasi;

    /**
     * TingkatPrestasiController constructor.
     * @param $tingkatprestasi
     */
    public function __construct()
    {
        $this->tingkatprestasi = new TingkatPrestasi();
    }

    public function index()
    {
        $tingkatprestasi = $this->tingkatprestasi->all();
        $data = [
            'success' => $tingkatprestasi['success'],
            "data"    => $tingkatprestasi['data'],
            "message" => $tingkatprestasi['message'],

        ];
        \Config\View::render('TingkatPrestasi.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('TingkatPrestasi.create', $data);
    }

    public function edit($id, $data = array())
    {
        $tingkatprestasi = $this->tingkatprestasi->detail($id);
        if (count($tingkatprestasi['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($tingkatprestasi['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $tingkatprestasi['message'],
                    "data"    => $tingkatprestasi['data'],
                ];
            } else {
                $arr = [
                    "success" => ($tingkatprestasi['success']),
                    "message" => $tingkatprestasi['message'],
                    "data"    => $tingkatprestasi['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('TingkatPrestasi.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Tingkat Prestasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->tingkatprestasi->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/tingkatprestasi'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> Tingkat Prestasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->tingkatprestasi->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/tingkatprestasi'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->tingkatprestasi->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/tingkatprestasi'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/tingkatprestasi'</script>";
        }
    }
}