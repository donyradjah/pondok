<?php

namespace Controller;

use Config\Config;
use Model\StatusSiswa;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class StatusSiswaController
{
    private $statussiswa;

    /**
     * StatusSiswaController constructor.
     * @param $statussiswa
     */
    public function __construct()
    {
        $this->statussiswa = new StatusSiswa();
    }

    public function index()
    {
        $statussiswa = $this->statussiswa->all();
        $data = [
            'success' => $statussiswa['success'],
            "data"    => $statussiswa['data'],
            "message" => $statussiswa['message'],

        ];
        \Config\View::render('StatusSiswa.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('StatusSiswa.create', $data);
    }

    public function edit($id, $data = array())
    {
        $statussiswa = $this->statussiswa->detail($id);
        if (count($statussiswa['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($statussiswa['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $statussiswa['message'],
                    "data"    => $statussiswa['data'],
                ];
            } else {
                $arr = [
                    "success" => ($statussiswa['success']),
                    "message" => $statussiswa['message'],
                    "data"    => $statussiswa['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('StatusSiswa.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> StatusSiswa" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->statussiswa->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/statussiswa'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> StatusSiswa" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->statussiswa->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/statussiswa'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->statussiswa->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/statussiswa'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/statussiswa'</script>";
        }
    }
}