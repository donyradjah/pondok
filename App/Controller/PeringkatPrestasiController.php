<?php

namespace Controller;

use Config\Config;
use Model\PeringkatPrestasi;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class PeringkatPrestasiController
{
    private $peringkatprestasi;

    /**
     * PeringkatPrestasiController constructor.
     * @param $peringkatprestasi
     */
    public function __construct()
    {
        $this->peringkatprestasi = new PeringkatPrestasi();
    }

    public function index()
    {
        $peringkatprestasi = $this->peringkatprestasi->all();
        $data = [
            'success' => $peringkatprestasi['success'],
            "data"    => $peringkatprestasi['data'],
            "message" => $peringkatprestasi['message'],

        ];
        \Config\View::render('PeringkatPrestasi.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('PeringkatPrestasi.create', $data);
    }

    public function edit($id, $data = array())
    {
        $peringkatprestasi = $this->peringkatprestasi->detail($id);
        if (count($peringkatprestasi['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($peringkatprestasi['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $peringkatprestasi['message'],
                    "data"    => $peringkatprestasi['data'],
                ];
            } else {
                $arr = [
                    "success" => ($peringkatprestasi['success']),
                    "message" => $peringkatprestasi['message'],
                    "data"    => $peringkatprestasi['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('PeringkatPrestasi.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> PeringkatPrestasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->peringkatprestasi->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/peringkatprestasi'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> PeringkatPrestasi" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->peringkatprestasi->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/peringkatprestasi'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->peringkatprestasi->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/peringkatprestasi'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/peringkatprestasi'</script>";
        }
    }
}