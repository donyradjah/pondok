<?php


namespace Controller;


use Config\View;
use Model\AlatTransportasi;
use Model\AsalSekolah;
use Model\BidangPrestasi;
use Model\CitaCita;
use Model\Hoby;
use Model\Jarak;
use Model\JenisPekerjaan;
use Model\JenisSekolah;
use Model\JenjangPendidikan;
use Model\Nilai;
use Model\Penghasilan;
use Model\PeringkatPrestasi;
use Model\Santri;
use Model\StatusLembaga;
use Model\StatusSiswa;
use Model\StatusWali;
use Model\TingkatPrestasi;
use PHPExcel_IOFactory;

class SantriController
{
    private $santri, $alattransportasi, $asalsekolah;
    private $bidangprestasi, $citacita, $hoby;
    private $jarak, $jenis_pekerjaan;
    private $jenis_sekolah;
    private $jenjang_pendidikan;
    private $penghasilan;
    private $peringkatprestasi;
    private $statuslembaga;
    private $statussiswa;
    private $statuswali;
    private $tingkatprestasi;
    private $nilai;


    /**
     * SantriController constructor.
     */
    function __construct()
    {
        $this->santri = new Santri();
        $this->alattransportasi = new AlatTransportasi();
        $this->asalsekolah = new AsalSekolah();
        $this->bidangprestasi = new BidangPrestasi();
        $this->citacita = new CitaCita();
        $this->hoby = new Hoby();
        $this->jarak = new Jarak();
        $this->jenis_pekerjaan = new JenisPekerjaan();
        $this->jenis_sekolah = new JenisSekolah();
        $this->jenjang_pendidikan = new JenjangPendidikan();
        $this->penghasilan = new Penghasilan();
        $this->peringkatprestasi = new PeringkatPrestasi();
        $this->statuslembaga = new StatusLembaga();
        $this->statussiswa = new StatusSiswa();
        $this->statuswali = new StatusWali();
        $this->tingkatprestasi = new TingkatPrestasi();
        $this->nilai = new Nilai();
    }

    public function index()
    {
        $data = $this->santri->all();

        View::render('Santri.all', $data);
    }

    public function excelall($id)
    {
        $statuslembaga = $this->statuslembaga->detail($id)['data']['nama'];
        $data = $this->nilai->allbystatuslembaga($id);
        $data['statuslembaga'] = $statuslembaga;
        $objPHPExcel = new \PHPExcel();

        $objPHPExcel->getProperties()->setCreator("Darun Najah")
            ->setLastModifiedBy("Darun Najah")
            ->setTitle("Darun Najah")
            ->setSubject($statuslembaga);
        // Set default font
        $objPHPExcel->getDefaultStyle()->getFont()->setName('Arial')
            ->setSize(10);
        //Set the first row as the header row
        $objPHPExcel->getActiveSheet()->setCellValue('A1', 'NIS')
            ->setCellValue('B1', 'Nama')
            ->setCellValue('C1', 'Adzan')
            ->setCellValue('D1', 'Sholat')
            ->setCellValue('E1', 'Al-Quran')
            ->setCellValue('F1', 'Status')
            ->setCellValue('G1', 'Nilai')
            ->setCellValue('H1', 'Ranking')
            ->setCellValue('I1', 'Kelas');
        //Rename the worksheet
        $objPHPExcel->getActiveSheet()->setTitle($statuslembaga);
        $objPHPExcel->setActiveSheetIndex(0);
        if (count($data['data']) > 0) {
            $i = 2;
            $j = 1;
            foreach ($data['data'] as $row) {

                if ($row['ranknil'] != NULL || $row['ranknil'] != "") {
                    $r = $j;
                    $j++;
                } else {
                    $r = "";
                }
                if ($row['ranknil'] != NULL || $row['ranknil'] != "") {
                    if ($i <= 20) {
                        $g = "A";
                    }
                    if ($i > 20 && $i <= 40) {
                        $g = "B";
                    }
                    if ($i > 40 && $i <= 60) {
                        $g = "C";
                    }
                }else {
                    $g = "";
                }
                $objPHPExcel->getActiveSheet()->setCellValue('A' . ($i), $row['tahunmasuk'] . "" . str_pad(($row['NIS']), 4, "0", STR_PAD_LEFT))
                    ->setCellValue('B' . ($i), $row['nama'])
                    ->setCellValue('C' . ($i), $row['adzan'])
                    ->setCellValue('D' . ($i), $row['sholat'])
                    ->setCellValue('E' . ($i), $row['quran'])
                    ->setCellValue('F' . ($i), $row['fo'])
                    ->setCellValue('G' . ($i), $row['ranknil'])
                    ->setCellValue('H' . ($i), $r)
                    ->setCellValue('I' . ($i), $g);
                $i++;;
            }
        }
        $filename = date('d-m-Y_H-i-s') . ".xlsx";
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
//if you want to save the file on the server instead of downloading,
//comment the last 3 lines and remove the comment from the next line
//$objWriter->save(str_replace('.php', '.xlsx', $filename));

//        header('Content-Type: application/json');
        header('Content-Type: application/vnd.ms-excel');
//        echo json_encode($data);
        header('Content-Disposition: attachment; filename=' . $filename);
        $objWriter->save("php://output");

    }

    public function allbystatuslembaga($id)
    {
        $statuslembaga = $this->statuslembaga->detail($id)['data']['nama'];
        $data = $this->santri->allstatuslembaga($id);
        $data['statuslembaga'] = $statuslembaga;

        View::render('Santri.list', $data);
    }

    public function indexbystatuslembaga($id)
    {
        $statuslembaga = $this->statuslembaga->detail($id)['data']['nama'];
        $data = $this->nilai->allbystatuslembaga($id);
        $data['statuslembaga'] = $statuslembaga;
        $data['statuslembagaid'] = $id;

        View::render('Santri.listnilai', $data);
    }

    public function store($data = array())
    {
        $insert = $this->santri->insert($data);
        if ($insert['success'] == false) {
            $this->create($insert);
        } else {
            echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/santri/statuslembaga/$data[status_lembaga]'</script>";
        }
    }

    public function create($data = array())
    {
        $data = [
            "alattransportasi"   => $this->alattransportasi->all()['data'],
            "asalsekolah"        => $this->asalsekolah->all()['data'],
            "bidangprestasi"     => $this->bidangprestasi->all()['data'],
            "citacita"           => $this->citacita->all()['data'],
            "hoby"               => $this->hoby->all()['data'],
            "jarak"              => $this->jarak->all()['data'],
            "jenis_pekerjaan"    => $this->jenis_pekerjaan->all()['data'],
            "jenis_sekolah"      => $this->jenis_sekolah->all()['data'],
            "jenjang_pendidikan" => $this->jenjang_pendidikan->all()['data'],
            "penghasilan"        => $this->penghasilan->all()['data'],
            "peringkatprestasi"  => $this->peringkatprestasi->all()['data'],
            "statuslembaga"      => $this->statuslembaga->all()['data'],
            "statussiswa"        => $this->statussiswa->all()['data'],
            "statuswali"         => $this->statuswali->all()['data'],
            "tingkatprestasi"    => $this->tingkatprestasi->all()['data'],
            "kota"               => $this->alattransportasi->kota()['data'],
            "provinsi"           => $this->alattransportasi->provinsi()['data'],
        ];
        View::render('Santri.create', $data);
    }

    public function update($id, $data = array())
    {
        $insert = $this->santri->update($id, $data);
        if ($insert['success'] == false) {
            $this->edit($insert);
        } else {
            echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/santri/statuslembaga/$data[status_lembaga]'</script>";
        }
    }

    public function edit($id, $data = array())
    {

        $statuswali = $this->santri->detail($id);

        if (count($statuswali['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success"            => ($statuswali['success'] && $data['success']),
                    "message"            => $data['message'] . "<br>" . $statuswali['message'],
                    "data"               => $statuswali['data'],
                    "alattransportasi"   => $this->alattransportasi->all()['data'],
                    "asalsekolah"        => $this->asalsekolah->all()['data'],
                    "bidangprestasi"     => $this->bidangprestasi->all()['data'],
                    "citacita"           => $this->citacita->all()['data'],
                    "hoby"               => $this->hoby->all()['data'],
                    "jarak"              => $this->jarak->all()['data'],
                    "jenis_pekerjaan"    => $this->jenis_pekerjaan->all()['data'],
                    "jenis_sekolah"      => $this->jenis_sekolah->all()['data'],
                    "jenjang_pendidikan" => $this->jenjang_pendidikan->all()['data'],
                    "penghasilan"        => $this->penghasilan->all()['data'],
                    "peringkatprestasi"  => $this->peringkatprestasi->all()['data'],
                    "statuslembaga"      => $this->statuslembaga->all()['data'],
                    "statussiswa"        => $this->statussiswa->all()['data'],
                    "statuswali"         => $this->statuswali->all()['data'],
                    "tingkatprestasi"    => $this->tingkatprestasi->all()['data'],
                    "kota"               => $this->alattransportasi->kota()['data'],
                    "provinsi"           => $this->alattransportasi->provinsi()['data'],
                ];
            } else {
                $arr = [
                    "success"            => ($statuswali['success']),
                    "message"            => $statuswali['message'],
                    "data"               => $statuswali['data'],
                    "alattransportasi"   => $this->alattransportasi->all()['data'],
                    "asalsekolah"        => $this->asalsekolah->all()['data'],
                    "bidangprestasi"     => $this->bidangprestasi->all()['data'],
                    "citacita"           => $this->citacita->all()['data'],
                    "hoby"               => $this->hoby->all()['data'],
                    "jarak"              => $this->jarak->all()['data'],
                    "jenis_pekerjaan"    => $this->jenis_pekerjaan->all()['data'],
                    "jenis_sekolah"      => $this->jenis_sekolah->all()['data'],
                    "jenjang_pendidikan" => $this->jenjang_pendidikan->all()['data'],
                    "penghasilan"        => $this->penghasilan->all()['data'],
                    "peringkatprestasi"  => $this->peringkatprestasi->all()['data'],
                    "statuslembaga"      => $this->statuslembaga->all()['data'],
                    "statussiswa"        => $this->statussiswa->all()['data'],
                    "statuswali"         => $this->statuswali->all()['data'],
                    "tingkatprestasi"    => $this->tingkatprestasi->all()['data'],
                    "kota"               => $this->alattransportasi->kota()['data'],
                    "provinsi"           => $this->alattransportasi->provinsi()['data'],
                ];
            }
        } else {
            $arr = [
                "success"            => false,
                "message"            => "Tidak dapat menemukan data dengan id " . $id,
                "alattransportasi"   => $this->alattransportasi->all()['data'],
                "asalsekolah"        => $this->asalsekolah->all()['data'],
                "bidangprestasi"     => $this->bidangprestasi->all()['data'],
                "citacita"           => $this->citacita->all()['data'],
                "hoby"               => $this->hoby->all()['data'],
                "jarak"              => $this->jarak->all()['data'],
                "jenis_pekerjaan"    => $this->jenis_pekerjaan->all()['data'],
                "jenis_sekolah"      => $this->jenis_sekolah->all()['data'],
                "jenjang_pendidikan" => $this->jenjang_pendidikan->all()['data'],
                "penghasilan"        => $this->penghasilan->all()['data'],
                "peringkatprestasi"  => $this->peringkatprestasi->all()['data'],
                "statuslembaga"      => $this->statuslembaga->all()['data'],
                "statussiswa"        => $this->statussiswa->all()['data'],
                "statuswali"         => $this->statuswali->all()['data'],
                "tingkatprestasi"    => $this->tingkatprestasi->all()['data'],
                "kota"               => $this->alattransportasi->kota()['data'],
                "provinsi"           => $this->alattransportasi->provinsi()['data'],
            ];
        }
        View::render('Santri.edit', $arr);
    }

    public function updatenilai($id, $data = array())
    {
        $insert = $this->nilai->update($id, $data);
        if ($insert['success'] == false) {
            $this->nilaicreate($id, $insert);
        } else {
            $this->hitung();
            echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/santri/nilai/$id'</script>";
        }
    }

    public function nilaicreate($id, $data = array())
    {
        $statuswali = $this->santri->detail($id);
        if (count($statuswali['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($statuswali['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $statuswali['message'],
                    "data"    => $statuswali['data'],
                    "nilai"   => $this->nilai->detail($id)['data'],

                ];
            } else {
                $arr = [
                    "success" => ($statuswali['success']),
                    "message" => $statuswali['message'],
                    "data"    => $statuswali['data'],
                    "nilai"   => $this->nilai->detail($id)['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }

        View::render('Santri.nilai', $arr);
    }

    /**
     * @return AlatTransportasi
     */
    public function hitung()
    {
        $this->nilai->truncatehasil();
        $this->nilai->truncateranking();
        $data = $this->nilai->all();
        $batas_rendah = (100 * 1) / 3;
        $batas_sedang = (100 * 2) / 3;
        $batas_tinggi = (100 * 3) / 3;
        $i = 0;
        $santrilolos = array();
        foreach ($data['data'] as $row) {
            $quran = $row['quran'];
            $sholat = $row['sholat'];
            $adzan = $row['adzan'];
            //mencari fungsi keanggotaan quran
            if ($quran > 0 && $quran <= $batas_rendah) {
                $quran1 = 1;
                $quran2 = 1;
                $quran_stat1 = 1;
                $quran_stat2 = 1;
            } elseif ($quran > $batas_rendah && $quran < $batas_sedang) {
                $quran1 = ($batas_sedang - $quran) / $batas_rendah;
                $quran2 = ($quran - $batas_rendah) / $batas_rendah;
                $quran_stat1 = 1;
                $quran_stat2 = 2;
            } elseif ($quran >= $batas_sedang && $quran < $batas_tinggi) {
                $quran1 = ($batas_tinggi - $quran) / $batas_rendah;
                $quran2 = ($quran - $batas_sedang) / $batas_rendah;
                $quran_stat1 = 2;
                $quran_stat2 = 3;
            } elseif ($quran >= $batas_tinggi) {
                $quran1 = 1;
                $quran2 = 1;
                $quran_stat1 = 3;
                $quran_stat2 = 3;
            } else {
                $quran1 = 0;
                $quran2 = 0;

            }
            //mencari fungsi keanggotaan sholat
            if ($sholat > 0 && $sholat <= $batas_rendah) {
                $sholat1 = 1;
                $sholat2 = 1;
                $sholat_stat1 = 1;
                $sholat_stat2 = 1;
            } elseif ($sholat > $batas_rendah && $sholat < $batas_sedang) {
                $sholat1 = ($batas_sedang - $sholat) / $batas_rendah;
                $sholat2 = ($sholat - $batas_rendah) / $batas_rendah;
                $sholat_stat1 = 1;
                $sholat_stat2 = 2;
            } elseif ($sholat >= $batas_sedang && $sholat < $batas_tinggi) {
                $sholat1 = ($batas_tinggi - $sholat) / $batas_rendah;
                $sholat2 = ($sholat - $batas_sedang) / $batas_rendah;
                $sholat_stat1 = 2;
                $sholat_stat2 = 3;
            } elseif ($sholat >= $batas_tinggi) {
                $sholat1 = 1;
                $sholat2 = 1;
                $sholat_stat1 = 3;
                $sholat_stat2 = 3;
            } else {
                $sholat1 = 0;
                $sholat2 = 0;

            }
            //mencari fungsi keanggotaan adzan
            if ($adzan > 0 && $adzan <= $batas_rendah) {
                $adzan1 = 1;
                $adzan2 = 1;
                $adzan_stat1 = 1;
                $adzan_stat2 = 1;
            } elseif ($adzan > $batas_rendah && $adzan < $batas_sedang) {
                $adzan1 = ($batas_sedang - $adzan) / $batas_rendah;
                $adzan2 = ($adzan - $batas_rendah) / $batas_rendah;
                $adzan_stat1 = 1;
                $adzan_stat2 = 2;
            } elseif ($adzan >= $batas_sedang && $adzan < $batas_tinggi) {
                $adzan1 = ($batas_tinggi - $adzan) / $batas_rendah;
                $adzan2 = ($adzan - $batas_sedang) / $batas_rendah;
                $adzan_stat1 = 2;
                $adzan_stat2 = 3;
            } elseif ($adzan >= $batas_tinggi) {
                $adzan1 = 1;
                $adzan2 = 1;
                $adzan_stat1 = 3;
                $adzan_stat2 = 3;
            } else {
                $adzan1 = 0;
                $adzan2 = 0;

            }
            //mengambil rule 1
            $r1 = $this->nilai->rulebase(array("quran" => $quran_stat1, "sholat" => $sholat_stat1, "adzan" => $adzan_stat1));
            //mengambil rule 2 jika ada
            $r2 = $this->nilai->rulebase(array("quran" => $quran_stat2, "sholat" => $sholat_stat2, "adzan" => $adzan_stat2));

            //mengambil apredikar rule 1
            $apr1 = min(array($quran1, $adzan1, $sholat1));
            //mengambil apredikar rule 2
            $apr2 = min(array($quran2, $adzan2, $sholat2));

            //hitung weight average
            $hasil_akhir = (($apr1 * $r1['data']['nilai']) + ($apr2 * $r2['data']['nilai'])) / ($apr1 + $apr2);
            $output = $this->nilai->rule();
            //menentukan hasil weight average layak atau tidak layak
            foreach ($output['data'] as $row2) {
                if ($hasil_akhir <= $row2['nilai']) {
                    $data['fo'] = $row2['fo'];
                    break;
                }
            }
            //jika weight average layak maka akan langsung menghitung profile matching / GAP
            if ($data['fo'] == "LAYAK") {
                //mengambil standar kriteria GAP
                $standargap = $this->nilai->getnilaigap()['data'];
                //konversi nilai santri dari range 0 -100 ke 1 - 5
                $adzanrange = $this->nilai->getrangegap($adzan)['data'];
                $sholatrange = $this->nilai->getrangegap($sholat)['data'];
               $quranrange = $this->nilai->getrangegap($quran)['data'];
               //mencari selisih dari hasil konversi nilai santri dengan standar kriteria
                $adzanselisih = $adzanrange['id'] - $standargap['adzan'];
                $sholatselisih = $sholatrange['id'] - $standargap['sholat'];
                $quranselisih = $quranrange['id'] - $standargap['quran'];
                //konversi selsisih nilai santri dengan standar kriteria ke bobot profile matching
                $adzankon = $this->nilai->getselisihgap($adzanselisih)['data'];
                $sholatkon = $this->nilai->getselisihgap($sholatselisih)['data'];
                $qurankon = $this->nilai->getselisihgap($quranselisih)['data'];
                //hitung ncf
                $ncf = ($sholatkon['bobot'] + $qurankon['bobot']) / 2;
                //hitung nsf
                $nsf = $adzankon['bobot'] / 2;
                //hasil akhir
                $nilai = (($ncf * 60) / 100) + (($nsf * 40) / 100);
                //update database
                $this->nilai->updaterank($row['santri_id'], $nilai);
            }
            $this->nilai->updatefo($row['santri_id'], $data);
            $i++;
        }

    }

    public function updategap($data = array())
    {
        $insert = $this->nilai->updategap($data);
        if ($insert['success'] == false) {
            $this->editgap($insert);
        } else {
            $this->hitung();
            echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/kriteriagap'</script>";
        }
    }

    public function editgap($data = array())
    {
        $statuswali = $this->nilai->getnilaigap();
        if (count($statuswali['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($statuswali['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $statuswali['message'],
                    "data"    => $statuswali['data'],
                    "nilai"   => $this->nilai->getnilaigap()['data'],

                ];
            } else {
                $arr = [
                    "success" => ($statuswali['success']),
                    "message" => $statuswali['message'],
                    "data"    => $statuswali['data'],
                    "nilai"   => $this->nilai->getnilaigap()['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }

        View::render('Santri.gap', $arr);
    }

    public function delete($id)
    {
        $statuswali = $this->santri->detail($id);
        $delete = $this->santri->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/santri/statuslembaga/" . $statuswali['data']['status_lembaga'] . "'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/santri/statuslembaga/" . $statuswali['data']['status_lembaga'] . "'</script>";
        }
    }
}