<?php

namespace Controller;

use Config\Config;
use Model\StatusLembaga;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;


class StatusLembagaController
{
    private $statuslembaga;

    /**
     * StatusLembagaController constructor.
     * @param $statuslembaga
     */
    public function __construct()
    {
        $this->statuslembaga = new StatusLembaga();
    }

    public function index()
    {
        $statuslembaga = $this->statuslembaga->all();
        $data = [
            'success' => $statuslembaga['success'],
            "data"    => $statuslembaga['data'],
            "message" => $statuslembaga['message'],

        ];
        \Config\View::render('StatusLembaga.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('StatusLembaga.create', $data);
    }

    public function edit($id, $data = array())
    {
        $statuslembaga = $this->statuslembaga->detail($id);
        if (count($statuslembaga['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($statuslembaga['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $statuslembaga['message'],
                    "data"    => $statuslembaga['data'],
                ];
            } else {
                $arr = [
                    "success" => ($statuslembaga['success']),
                    "message" => $statuslembaga['message'],
                    "data"    => $statuslembaga['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('StatusLembaga.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('nama', validator::notEmpty())
            ->key('nama', validator::stringType()->length(1, 50))
            ->key('kode', validator::notEmpty())
            ->key('kode', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> StatusLembaga" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->statuslembaga->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/statuslembaga'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('nama', validator::notEmpty())
            ->key('nama', validator::stringType()->length(1, 50))
            ->key('kode', validator::notEmpty())
            ->key('kode', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> StatusLembaga" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->statuslembaga->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/statuslembaga'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->statuslembaga->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/statuslembaga'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/statuslembaga'</script>";
        }
    }
}