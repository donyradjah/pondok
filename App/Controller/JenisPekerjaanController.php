<?php

namespace Controller;

use Config\Config;
use Model\JenisPekerjaan;
use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator as validator;



class JenisPekerjaanController
{
    private $jenis_pekerjaan;

    /**
     * JenisPekerjaanController constructor.
     * @param $jenis_pekerjaan
     */
    public function __construct()
    {
        $this->jenis_pekerjaan = new JenisPekerjaan();
    }

    public function index()
    {
        $jenis_pekerjaan = $this->jenis_pekerjaan->all();
        $data = [
            'success' => $jenis_pekerjaan['success'],
            "data"    => $jenis_pekerjaan['data'],
            "message" => $jenis_pekerjaan['message'],

        ];
        \Config\View::render('JenisPekerjaan.list', $data);
    }

    public function create($data = array())
    {
        \Config\View::render('JenisPekerjaan.create', $data);
    }

    public function edit($id, $data = array())
    {
        $jenis_pekerjaan = $this->jenis_pekerjaan->detail($id);
        if (count($jenis_pekerjaan['data']) > 0) {
            if (isset($data['success'])) {
                $arr = [
                    "success" => ($jenis_pekerjaan['success'] && $data['success']),
                    "message" => $data['message'] . "<br>" . $jenis_pekerjaan['message'],
                    "data"    => $jenis_pekerjaan['data'],
                ];
            } else {
                $arr = [
                    "success" => ($jenis_pekerjaan['success']),
                    "message" => $jenis_pekerjaan['message'],
                    "data"    => $jenis_pekerjaan['data'],
                ];
            }
        } else {
            $arr = [
                "success" => false,
                "message" => "Tidak dapat menemukan data dengan id " . $id,
            ];
        }
        \Config\View::render('JenisPekerjaan.edit', $arr);
    }

    public function store($data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> JenisPekerjaan" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->create($arr);
        }

        if ($valid) {
            $insert = $this->jenis_pekerjaan->insert($data);
            if ($insert['success'] == false) {
                $this->create($insert);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jenispekerjaan'</script>";
            }
        }
    }

    public function update($id, $data = array())
    {

        $validator = validator::key('deskripsi', validator::notEmpty())
            ->key('deskripsi', validator::stringType()->length(1, 50));
        try {
            $valid = $validator->assert($data);
        } catch (NestedValidationException $exception) {

            $message = "<ul>";
            foreach ($exception->findMessages(Config::error_message_data_master()) as $r) {
                if ($r != "") {
                    $message .= "<li> JenisPekerjaan" . $r . "</li>";
                }
            }
            $message .= "</ul>";

            $arr = [
                "success" => false,
                "message" => $message,
                "data"    => $data,
            ];
            $this->edit($id, $arr);
        }

        if ($valid) {
            $update = $this->jenis_pekerjaan->update($id, $data);
            if ($update['success'] == false) {
                $this->edit($id, $update);
            } else {
                echo "<script type='text/javascript'>alert('data berhasil di simpan');document.location='" . URLS . "/jenispekerjaan'</script>";
            }
        }
    }

    public function delete($id)
    {
        $delete = $this->jenis_pekerjaan->delete($id);
        if ($delete['success'] == true) {
            echo "<script type='text/javascript'>alert('data berhasil di hapus');document.location='" . URLS . "/jenispekerjaan'</script>";
        } else {
            echo "<script type='text/javascript'>alert('" . $delete['message'] . "');document.location='" . URLS . "/jenispekerjaan'</script>";
        }
    }
}