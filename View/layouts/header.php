<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?php echo URLS ?>/assets/images/favicon.ico">
    <!-- App title -->
    <title>Pendaftaran Santri</title>

    <!-- DataTables -->
    <link href="<?php echo URLS ?>/assets/plugins/datatables/jquery.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/buttons.bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/fixedHeader.bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/responsive.bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/scroller.bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/dataTables.colVis.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/dataTables.bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/datatables/fixedColumns.dataTables.min.css" rel="stylesheet"
          type="text/css"/>
    <link href="<?php echo URLS ?>/assets/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo URLS ?>/assets/plugins/multiselect/css/multi-select.css"  rel="stylesheet" type="text/css" />
    <link href="<?php echo URLS ?>/assets/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo URLS ?>/assets/plugins/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo URLS ?>/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="<?php echo URLS ?>/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <!-- App css -->
    <link href="<?php echo URLS ?>/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/css/core.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/css/components.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/css/icons.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/css/pages.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/css/menu.css" rel="stylesheet" type="text/css"/>
    <link href="<?php echo URLS ?>/assets/css/responsive.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.css">
    <style class="example">
        .form-section {

            display: none;
        }
        .form-section.current {
            display: inherit;
        }
        .btn-info, .btn-default {
            margin-top: 10px;
        }
    </style>
    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?php echo URLS ?>/assets/js/modernizr.min.js"></script>

</head>


<body class="fixed-left">

<!-- Loader -->
<div id="preloader">
    <div id="status">
        <div class="spinner">
            <div class="spinner-wrapper">
                <div class="rotator">
                    <div class="inner-spin"></div>
                    <div class="inner-spin"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Begin page -->
<div id="wrapper">

    <!-- Top Bar Start -->
    <div class="topbar">

        <!-- LOGO -->
        <div class="topbar-left">
            <a href="index.html" class="logo"><span>Darun Najah</span><i class="mdi mdi-cube"></i></a>
            <!-- Image logo -->
            <!--<a href="index.html" class="logo">-->
            <!--<span>-->
            <!--<img src="assets/images/logo.png" alt="" height="30">-->
            <!--</span>-->
            <!--<i>-->
            <!--<img src="assets/images/logo_sm.png" alt="" height="28">-->
            <!--</i>-->
            <!--</a>-->
        </div>

        <!-- Button mobile view to collapse sidebar menu -->
        <div class="navbar navbar-default" role="navigation">
            <div class="container">

                <!-- Navbar-left -->
                <ul class="nav navbar-nav navbar-left">
                    <li>
                        <button class="button-menu-mobile open-left waves-effect waves-light">
                            <i class="mdi mdi-menu"></i>
                        </button>
                    </li>
                </ul>

                <!-- Right(Notification) -->
                <ul class="nav navbar-nav navbar-right">


                    <li class="dropdown user-box">
                        <a href="" class="dropdown-toggle waves-effect waves-light user-link" data-toggle="dropdown"
                           aria-expanded="true">
                            <img src="<?php echo URLS ?>/assets/images/user.png" alt="user-img"
                                 class="img-circle user-img">
                        </a>

                        <ul class="dropdown-menu dropdown-menu-right arrow-dropdown-menu arrow-menu-right user-list notify-list">
                            <li>
                                <h5><?php echo $_SESSION['nama'] ?> </h5>
                            </li>
                            <li><a href="<?php echo URLS ?>/user/update/<?php echo $_SESSION['id'] ?>"><i
                                            class="ti-user m-r-5"></i> Profile</a></li>
                            <li><a href="<?php echo URLS ?>/user/updatepass/<?php echo $_SESSION['id'] ?>"><i
                                            class="fa fa-key m-r-5"></i> Ubah password</a></li>
                            <li><a href="<?php echo URLS ?>/logout"><i class="ti-power-off m-r-5"></i> Logout</a></li>
                        </ul>
                    </li>

                </ul> <!-- end navbar-right -->

            </div><!-- end container -->
        </div><!-- end navbar -->
    </div>
    <!-- Top Bar End -->
