<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">

        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <div class="user-details">
                <div class="overlay"></div>
                <div class="text-center">
                    <img src="<?php echo URLS ?>/assets/images/user.png" alt="" class="thumb-md img-circle">
                </div>
                <div class="user-info">
                    <div>
                        <a href="#setting-dropdown" class="dropdown-toggle" data-toggle="dropdown"
                           aria-expanded="false"><?php echo $_SESSION['nama'] . " - " . $_SESSION['email'] ?> <span
                                    class="mdi mdi-menu-down"></span></a>
                    </div>
                </div>
            </div>

            <div class="dropdown" id="setting-dropdown">
                <ul class="dropdown-menu">
                    <li><a href="<?php echo URLS ?>/logout"><i class="mdi mdi-logout m-r-5"></i> Logout</a></li>
                </ul>
            </div>

            <ul>
                <li class="menu-title">Navigation</li>

                <li>
                    <a href="<?php echo URLS ?>/" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span>
                    </a>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi mdi-account-circle"></i>
                        <span> Data Santri </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo URLS ?>/santri/statuslembaga/1">Madrasah Aliyah</a></li>
                        <li><a href="<?php echo URLS ?>/santri/statuslembaga/2">Madrasah Tsanawiyah</a></li>
                        <li><a href="<?php echo URLS ?>/santri/statuslembaga/3">Madrasah Diniyah</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tasks"></i>
                        <span> Proccessing </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo URLS ?>/kriteriagap">Standar Kriteria</a></li>

                        <li><a href="<?php echo URLS ?>/santri/listnilai/1">Madrasah Aliyah</a></li>
                        <li><a href="<?php echo URLS ?>/santri/listnilai/2">Madrasah Tsanawiyah</a></li>
                        <li><a href="<?php echo URLS ?>/santri/listnilai/3">Madrasah Diniyah</a></li>


                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-tasks"></i>
                        <span> Data Master </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo URLS ?>/alattransportasi">Alat Transporatasi</a></li>
                        <li><a href="<?php echo URLS ?>/asalsekolah">Asal Sekolah</a></li>
                        <li><a href="<?php echo URLS ?>/bidangprestasi">Bidang Prestasi</a></li>
                        <li><a href="<?php echo URLS ?>/citacita">Cita - Cita</a></li>
                        <li><a href="<?php echo URLS ?>/hoby">Hoby</a></li>
                        <li><a href="<?php echo URLS ?>/jarak">Jarak</a></li>
                        <li><a href="<?php echo URLS ?>/jenispekerjaan">Jenis Pekerjaan</a></li>
                        <li><a href="<?php echo URLS ?>/jenissekolah">Jenis Sekolah</a></li>
                        <li><a href="<?php echo URLS ?>/jenjangpendidikan">Jenjang Pendidikan</a></li>
                        <li><a href="<?php echo URLS ?>/penghasilan">Penghasilan</a></li>
                        <li><a href="<?php echo URLS ?>/peringkatprestasi">Peringkat Prestasi</a></li>
                        <li><a href="<?php echo URLS ?>/statuslembaga">Status Lembaga</a></li>
                        <li><a href="<?php echo URLS ?>/statussiswa">Status Siswa</a></li>
                        <li><a href="<?php echo URLS ?>/statuswali">Status Wali</a></li>
                        <li><a href="<?php echo URLS ?>/tingkatprestasi">Tingkat Prestasi</a></li>
                    </ul>
                </li>
                <?php
                if ($_SESSION['level'] > 100) { ?>
                    <li>
                        <a href="<?php echo URLS ?>/user" class="waves-effect"><i class="mdi mdi mdi-account-box"></i>
                            <span> Data User </span>
                        </a>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <!-- Sidebar -->
        <div class="clearfix"></div>


    </div>
    <!-- Sidebar -left -->

</div>
<!-- Left Sidebar End -->