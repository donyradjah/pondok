<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->

<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Santri </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li class="active">
                                <a href="#">Data Santri</a>
                            </li>

                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">


                        <h4 class="header-title m-t-0">Tambah Daftar Santri</h4>
                        <?php
                        if ($success == false && isset($success)) {
                            ?>
                            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">

                                <i class="mdi mdi-block-helper"></i>
                                <strong>Terjadi Kesalahan - </strong> <?php echo $message ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="p-20">

                            <form class="demo-form form-horizontal" role="form" method="post">
                                <div class="form-section">
                                    <h2>Data Santri</h2>
                                    <div class="form-group">
                                        <label for="status_lembaga">Status lembaga:</label>
                                        <select parsley-trigger="change" name="status_lembaga" id="status_lembaga"
                                                class="form-control selectpicker" data-live-search="true" required>
                                            <option value="">-- Pilih Status lembaga --</option>
                                            <?php foreach ($statuslembaga as $row) {
                                                ?>
                                                <option <?php if ($data['status_lembaga'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['nama'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="NISN">NISN:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-length="[10, 10]" data-parsley-type="number"
                                               value="<?php echo $data['NISN'] ?>"
                                               placeholder="Diisi dengan Nomor Induk Siswa Nasional (NISN) dari siswa (terdiri dari 10 digit yang diterbitkan secara resmi oleh Kemdikbud)."
                                               id="NISN" class="form-control"
                                               name="NISN">
                                    </div>


                                    <div class="form-group">
                                        <label for="NIK">NIK:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-length="[16, 16]" data-parsley-type="number"
                                               value="<?php echo $data['NIK'] ?>"
                                               placeholder="Diisi dengan Nomor Induk Kependudukan (NIK) dari siswa yang bersangkutan (yang diterbitkan secara resmi oleh Dinas Kependudukan)."
                                               id="NIK" class="form-control"
                                               name="NIK" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama">Nama:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-length="[5, 50]" id="nama" class="form-control"
                                               value="<?php echo $data['nama'] ?>"
                                               placeholder="Diisi dengan Nama Lengkap siswa"
                                               name="nama" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="tempat_lahir">Tempat Lahir:</label>
                                        <select parsley-trigger="change" name="tempat_lahir" id="tempat_lahir" class="form-control selectpicker"
                                                data-live-search="true" required>
                                            <option value="">-- Pilih Kota Tempat Lahir --</option>
                                            <?php foreach ($kota as $row) {
                                                ?>
                                                <option <?php if ($data['tempat_lahir'] == $row['kota']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['kota'] ?>"><?php echo $row['kota'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_lahir">Tanggal Lahir:</label>
                                        <input type="text" parsley-trigger="change" required class="form-control"
                                               value="<?php echo $data['tanggal_lahir'] ?>"
                                               name="tanggal_lahir"
                                               placeholder="Diisi dengan Tanggal Lahir siswa (format : DD/MM/YYYY). Contoh: 12 Juli 1998, ditulis : 12/07/1998"
                                               id="datepicker">
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_kelamin">Jenis Kelamin:</label>
                                        <select parsley-trigger="change" name="jenis_kelamin" id="jenis_kelamin"
                                                class="form-control selectpicker"
                                                required>
                                            <option value="">-- Pilih Jenis Kelamin --</option>
                                            <option <?php if ($data['jenis_kelamin'] == "L") {
                                                echo "selected";
                                            } ?> value="L">laki - laki
                                            </option>
                                            <option <?php if ($data['jenis_kelamin'] == "P") {
                                                echo "selected";
                                            } ?> value="L">Perempuan
                                            </option>

                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="hoby">Hoby:</label>
                                        <select parsley-trigger="change" name="hoby" id="hoby" class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Hoby --</option>
                                            <?php foreach ($hoby as $row) {
                                                ?>
                                                <option <?php if ($data['hoby'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="cita_cita">Cita Cita:</label>
                                        <select parsley-trigger="change" name="cita_cita" id="cita_cita" class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Cita Cita --</option>
                                            <?php foreach ($citacita as $row) {
                                                ?>
                                                <option <?php if ($data['cita_cita'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jumlah_saudara">Jumlah Saudara kandung:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-type="number"
                                               value="<?php echo $data['jumlah_saudara'] ?>"
                                               placeholder="Diisi dengan Jumlah Saudara Kandung dari siswa yang bersangkutan."
                                               id="jumlah_saudara" class="form-control"
                                               name="jumlah_saudara">
                                    </div>
                                    <div class="form-group">
                                        <label for="status_siswa">Status Siswa:</label>
                                        <select parsley-trigger="change" name="status_siswa" id="status_siswa" class="form-control selectpicker"
                                                data-live-search="true" required>
                                            <option value="">-- Pilih Status Siswa --</option>
                                            <?php foreach ($statussiswa as $row) {
                                                ?>
                                                <option <?php if ($data['status_siswa'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-section">
                                    <h2>Data Sekolah</h2>
                                    <div class="form-group">
                                        <label for="asal_sekolah"> Asal Sekolah Sebelumnya:</label>
                                        <select parsley-trigger="change" name="asal_sekolah" id="asal_sekolah" class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Asal Sekolah --</option>
                                            <?php foreach ($asalsekolah as $row) {
                                                ?>
                                                <option <?php if ($data['asal_sekolah'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_sekolah"> Jenis Sekolah Jenjang Sebelumnya :</label>
                                        <select parsley-trigger="change" name="jenis_sekolah" id="jenis_sekolah"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Jenis Sekolah --</option>
                                            <?php foreach ($jenis_sekolah as $row) {
                                                ?>
                                                <option <?php if ($data['jenis_sekolah'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status_sekolah"> Status Sekolah Jenjang Sebelumnya :</label>
                                        <select parsley-trigger="change" name="status_sekolah" id="status_sekolah"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Status Sekolah --</option>
                                            <option <?php if ($data['status_sekolah'] == "Negeri") {
                                                echo "selected";
                                            } ?> value="Negeri">Negeri
                                            </option>
                                            <option <?php if ($data['status_sekolah'] == "Swasta") {
                                                echo "selected";
                                            } ?> value="Swasta">Swasta
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_kabupaten_sekolah">Kabupaten/Kota lokasi Sekolah Jenjang
                                            Sebelumnyar :</label>
                                        <select parsley-trigger="change" name="nama_kabupaten_sekolah" id="nama_kabupaten_sekolah"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Kabupaten/Kota lokasi Sekolah Jenjang --</option>
                                            <?php foreach ($kota as $row) {
                                                ?>
                                                <option <?php if ($data['nama_kabupaten_sekolah'] == $row['kota']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['kota'] ?>"><?php echo $row['kota'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_skhun">Nomor Peserta/SKHUN siswa:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-length="[20, 20]"
                                               value="<?php echo $data['nomor_skhun'] ?>"
                                               placeholder="Diisi dengan 20 Karakter Nomor Peserta/SKHUN siswa yang bersangkutan pada saat menyelesaikan jenjang SMP/MTs/Sederajat"
                                               id="nomor_skhun" class="form-control"
                                               name="nomor_skhun">
                                    </div>
                                    <div class="form-group">
                                        <label for="NPSN">NPSN Sekolah Jenjang Sebelumnya:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-length="[8, 10]"
                                               value="<?php echo $data['NPSN'] ?>"
                                               placeholder="Diisi dengan NPSN Sekolah Jenjang Sebelumnya (jenjang SMP/MTs/Sederajat) dari siswa yang bersangkutan."
                                               id="NPSN" class="form-control"
                                               name="NPSN">
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_blanko_skhun">Nomor Blangko SKHUN Sekolah Jenjang
                                            Sebelumnya:</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nomor_blanko_skhun'] ?>"
                                               placeholder="Diisi dengan Nomor Blangko SKHUN Sekolah Jenjang Sebelumnya (jenjang SMP/MTs/Sederajat) dari siswa yang bersangkutan."
                                               id="nomor_blanko_skhun" class="form-control"
                                               name="nomor_blanko_skhun">
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_ijazah">Nomor Seri Ijazah Sekolah Jenjang Sebelumnya:</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nomor_ijazah'] ?>"
                                               placeholder="Diisi dengan Nomor Seri Ijazah Sekolah Jenjang Sebelumnya (jenjang SMP/MTs/Sederajat) dari siswa yang bersangkutan."
                                               id="nomor_ijazah" class="form-control"
                                               name="nomor_ijazah">
                                    </div>
                                    <div class="form-group">
                                        <label for="total_nilai">Total Nilai yang diperoleh lulusan pada Ujian Nasional
                                            (UN) pada jenjang sebelumnya:</label>
                                        <input type="text" parsley-trigger="change" data-parsley-pattern="^[0-9]*\.^[0-9]$"
                                               value="<?php echo $data['total_nilai'] ?>"
                                               placeholder="Diisi dengan Total Nilai yang diperoleh lulusan pada Ujian Nasional (UN) pada jenjang sebelumnya (SMP/MTs/Sederajat)"
                                               id="total_nilai" class="form-control"
                                               name="total_nilai">
                                    </div>
                                    <div class="form-group">
                                        <label for="tanggal_lulus">Tanggal Kelulusan Siswa di Jenjang
                                            sebelumnya:</label>
                                        <input type="text" parsley-trigger="change" class="form-control"
                                               value="<?php echo $data['tanggal_lulus'] ?>"
                                               name="tanggal_lulus"
                                               placeholder="Diisi dengan Tanggal Kelulusan Siswa di Jenjang sebelumnya (format : DD/MM/YYYY). Contoh: 12 Juli 1998, ditulis : 12/07/1998"
                                               id="datepicker">
                                    </div>
                                </div>

                                <div class="form-section">
                                    <h2>Data Orang tua/Wali</h2>
                                    <div class="form-group">
                                        <label for="alamat_ortu"> Alamat Tempat Tinggal Orangtua/Wali Siswa :</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['alamat_ortu'] ?>"
                                               placeholder="Diisi dengan Alamat Tempat Tinggal Orangtua/Wali Siswa."
                                               id="alamat_ortu" class="form-control"
                                               name="alamat_ortu">
                                    </div>
                                    <div class="form-group">
                                        <label for="provinsi_ortu">Provinsi Tempat Tinggal Orangtua/Wali Siswa:</label>
                                        <select parsley-trigger="change" name="provinsi_ortu" id="provinsi_ortu"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Provinsi Tempat Tinggal Orangtua/Wali Siswa --
                                            </option>
                                            <?php foreach ($provinsi as $row) {
                                                ?>
                                                <option <?php if ($data['provinsi_ortu'] == $row['provinsi']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['provinsi'] ?>"><?php echo $row['provinsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_kabupaten_ortu">Nama Kabupaten/Kota Tempat Tinggal
                                            Orangtua/Wali Siswa :</label>
                                        <select parsley-trigger="change" name="nama_kabupaten_ortu" id="nama_kabupaten_ortu"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Nama Kabupaten/Kota Tempat Tinggal Orangtua/Wali
                                                Siswa --
                                            </option>
                                            <?php foreach ($kota as $row) {
                                                ?>
                                                <option <?php if ($data['nama_kabupaten_ortu'] == $row['kota']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['kota'] ?>"><?php echo $row['kota'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_kecamatan_ortu"> Nama Kecamatan Tempat Tinggal Orangtua/Wali
                                            Siswa :</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nama_kecamatan_ortu'] ?>"
                                               placeholder="Diisi dengan Nama Kecamatan Tempat Tinggal Orangtua/Wali Siswa."
                                               id="nama_kecamatan_ortu" class="form-control"
                                               name="nama_kecamatan_ortu">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_desa_ortu"> Nama Desa Tempat Tinggal Orangtua/Wali
                                            Siswa :</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nama_desa_ortu'] ?>"
                                               placeholder="Diisi dengan Nama Desa/Kelurahan Tempat Tinggal Orangtua/Wali Siswa."
                                               id="nama_desa_ortu" class="form-control"
                                               name="nama_desa_ortu">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_desa_ortu"> Kode Pos Tempat Tinggal Orangtua/Wali Siswa
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[3, 6]" data-parsley-type="number"
                                               value="<?php echo $data['kode_pos_ortu'] ?>"
                                               placeholder="Diisi dengan Kode Pos Tempat Tinggal Orangtua/Wali Siswa."
                                               id="kode_pos_ortu" class="form-control"
                                               name="kode_pos_ortu">
                                    </div>
                                    <div class="form-group">
                                        <label for="jarak"> Jarak Tempat Tinggal Siswa Ke Madrasah:</label>
                                        <select parsley-trigger="change" name="jarak" id="jarak"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Jarak Tempat Tinggal Siswa Ke Madrasah --
                                            </option>
                                            <?php foreach ($jarak as $row) {
                                                ?>
                                                <option <?php if ($data['jarak'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="alattransportasi"> Alat Transportasi yang digunakan siswa dari
                                            tempat tinggal menuju madrasah:</label>
                                        <select parsley-trigger="change" name="alattransportasi" id="alattransportasi"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Alat Transportasi yang digunakan siswa dari tempat
                                                tinggal menuju madrasah --
                                            </option>
                                            <?php foreach ($alattransportasi as $row) {
                                                ?>
                                                <option <?php if ($data['alattransportasi'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_kk"> Nomor Kartu Keluarga yang dimiliki oleh orangtua siswa
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[3, 6]" data-parsley-type="number"
                                               value="<?php echo $data['nomor_kk'] ?>"
                                               placeholder="Diisi dengan Nomor Kartu Keluarga yang dimiliki oleh orangtua siswa."
                                               id="nomor_kk" class="form-control"
                                               name="nomor_kk">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_ayah"> Nama Lengkap Ayah Kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[3, 35]"
                                               value="<?php echo $data['nama_ayah'] ?>"
                                               placeholder="Diisi dengan Nama Lengkap Ayah Kandung dari siswa bersangkutan."
                                               id="nama_ayah" class="form-control"
                                               name="nama_ayah">
                                    </div>
                                    <div class="form-group">
                                        <label for="NIK_ayah"> Nomor Induk Kependudukan (NIK) atau Nomor KTP dari Ayah
                                            kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[16, 16]"
                                               data-parsley-type="number"
                                               value="<?php echo $data['NIK_ayah'] ?>"
                                               placeholder="Diisi dengan Nomor Induk Kependudukan (NIK) atau Nomor KTP dari Ayah kandung dari siswa bersangkutan."
                                               id="NIK_ayah" class="form-control"
                                               name="NIK_ayah">
                                    </div>
                                    <div class="form-group">
                                        <label for="jenjang_pendidikan_ayah"> Jenjang Pendidikan Terakhir Ayah Kandung
                                            dari siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="jenjang_pendidikan_ayah" id="jenjang_pendidikan_ayah"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Jenjang Pendidikan Terakhir Ayah Kandung dari
                                                siswa bersangkutan --
                                            </option>
                                            <?php foreach ($jenjang_pendidikan as $row) {
                                                ?>
                                                <option <?php if ($data['jenjang_pendidikan_ayah'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_pekerjaan_ayah"> Pekerjaan Ayah Kandung dari siswa
                                            bersangkutan dari siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="jenis_pekerjaan_ayah" id="jenis_pekerjaan_ayah"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Pekerjaan Ayah Kandung dari siswa bersangkutan
                                                dari siswa bersangkutan --
                                            </option>
                                            <?php foreach ($jenis_pekerjaan as $row) {
                                                ?>
                                                <option <?php if ($data['jenis_pekerjaan_ayah'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="no_hp_ayah"> Nomor Handphone dari Ayah
                                            kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[8, 12]"
                                               data-parsley-type="number"
                                               value="<?php echo $data['no_hp_ayah'] ?>"
                                               placeholder="Diisi dengan Nomor Handphone dari Ayah kandung dari siswa bersangkutan."
                                               id="no_hp_ayah" class="form-control"
                                               name="no_hp_ayah">
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_ibu"> Nama Lengkap Ibu Kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[3, 35]"
                                               value="<?php echo $data['nama_ibu'] ?>"
                                               placeholder="Diisi dengan Nama Lengkap Ibu Kandung dari siswa bersangkutan."
                                               id="nama_ibu" class="form-control"
                                               name="nama_ibu">
                                    </div>
                                    <div class="form-group">
                                        <label for="NIK_ibu"> Nomor Induk Kependudukan (NIK) atau Nomor KTP dari Ibu
                                            kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[16, 16]"
                                               data-parsley-type="number"
                                               value="<?php echo $data['NIK_ibu'] ?>"
                                               placeholder="Diisi dengan Nomor Induk Kependudukan (NIK) atau Nomor KTP dari Ibu kandung dari siswa bersangkutan."
                                               id="NIK_ibu" class="form-control"
                                               name="NIK_ibu">
                                    </div>
                                    <div class="form-group">
                                        <label for="jenjang_pendidikan_ibu"> Jenjang Pendidikan Terakhir Ibu Kandung
                                            dari siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="jenjang_pendidikan_ibu" id="jenjang_pendidikan_ayah"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Jenjang Pendidikan Terakhir Ibu Kandung dari siswa
                                                bersangkutan --
                                            </option>
                                            <?php foreach ($jenjang_pendidikan as $row) {
                                                ?>
                                                <option <?php if ($data['jenjang_pendidikan_ibu'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_pekerjaan_ibu"> Pekerjaan Ibu Kandung dari siswa
                                            bersangkutan dari siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="jenis_pekerjaan_ibu" id="jenis_pekerjaan_ibu"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Pekerjaan Ibu Kandung dari siswa bersangkutan
                                                dari siswa bersangkutan --
                                            </option>
                                            <?php foreach ($jenis_pekerjaan as $row) {
                                                ?>
                                                <option <?php if ($data['jenis_pekerjaan_ibu'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="no_hp_ibu"> Nomor Handphone dari Ibu
                                            kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[8, 12]"
                                               data-parsley-type="number"
                                               value="<?php echo $data['no_hp_ibu'] ?>"
                                               placeholder="Diisi dengan Nomor Handphone dari Ibu kandung dari siswa bersangkutan."
                                               id="no_hp_ibu" class="form-control"
                                               name="no_hp_ibu">
                                    </div>
                                    <div class="form-group">
                                        <label for="rata_penghasilan_ortu"> Rata-Rata Penghasilan Orangtua per Bulan
                                            dari
                                            siswa bersangkutan (penghasilan gabungan ayah & ibu):</label>
                                        <select parsley-trigger="change" name="rata_penghasilan_ortu" id="rata_penghasilan_ortu"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Rata-Rata Penghasilan Orangtua per Bulan dari
                                                siswa
                                                bersangkutan (penghasilan gabungan ayah & ibu) --
                                            </option>
                                            <?php foreach ($penghasilan as $row) {
                                                ?>
                                                <option <?php if ($data['rata_penghasilan_ortu'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama_wali"> Nama Lengkap Wali Siswa (jika yang
                                            bertanggung jawab dalam pendidikan siswa yang bersangkutan bukan orangtua
                                            kandungnya).
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[3, 35]"
                                               value="<?php echo $data['nama_wali'] ?>"
                                               placeholder="Diisi dengan Nama Lengkap Wali Siswa (jika yang bertanggung jawab dalam pendidikan siswa yang bersangkutan bukan orangtua kandungnya)."
                                               id="nama_wali" class="form-control"
                                               name="nama_wali">
                                    </div>
                                    <div class="form-group">
                                        <label for="NIK_wali"> Nomor Induk Kependudukan (NIK) atau Nomor KTP dari Wali
                                            kandung dari siswa bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[16, 16]"
                                               data-parsley-type="number"
                                               value="<?php echo $data['NIK_wali'] ?>"
                                               placeholder="Diisi dengan Nomor Induk Kependudukan (NIK) atau Nomor KTP  Wali dari siswa bersangkutan."
                                               id="NIK_wali" class="form-control"
                                               name="NIK_wali">
                                    </div>
                                    <div class="form-group">
                                        <label for="tahun_lahir_wali">Tahun Lahir Wali:</label>
                                        <input type="text" parsley-trigger="change"  class="form-control datepickeryear"
                                               value="<?php echo $data['tahun_lahir_wali'] ?>"
                                               name="tahun_lahir_wali"
                                               placeholder="Diisi dengan Tahun Lahir Wali Siswa yang bersangkutan. Contoh : Jika kelahiran tahun 1970, maka ditulis : 1970"
                                               id="tahun_lahir_wali">
                                    </div>
                                    <div class="form-group">
                                        <label for="jenjang_pendidikan_wali"> Jenjang Pendidikan Terakhir Wali dari
                                            siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="jenjang_pendidikan_wali" id="jenjang_pendidikan_wali"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Jenjang Pendidikan Terakhir Wali dari siswa
                                                bersangkutan --
                                            </option>
                                            <?php foreach ($jenjang_pendidikan as $row) {
                                                ?>
                                                <option <?php if ($data['jenjang_pendidikan_wali'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis_pekerjaan_wali"> Pekerjaan Wali dari siswa
                                            bersangkutan:</label>
                                        <select parsley-trigger="change" name="jenis_pekerjaan_wali" id="jenis_pekerjaan_wali"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Pekerjaan Wali dari siswa bersangkutan --
                                            </option>
                                            <?php foreach ($jenis_pekerjaan as $row) {
                                                ?>
                                                <option <?php if ($data['jenis_pekerjaan_wali'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="status_wali"> Status Wali dari siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="status_wali" id="status_wali"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih PStatus Wali dari siswa bersangkutan --
                                            </option>
                                            <?php foreach ($statuswali as $row) {
                                                ?>
                                                <option <?php if ($data['status_wali'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="no_hp_wali">No Handphone Wali Siswa yang bersangkutan
                                            :</label>
                                        <input type="text" parsley-trigger="change"
                                               data-parsley-length="[8, 12]"
                                               data-parsley-type="number"
                                               value="<?php echo $data['no_hp_wali'] ?>"
                                               placeholder="Diisi dengan No Handphone Wali Siswa yang bersangkutan."
                                               id="no_hp_wali" class="form-control"
                                               name="no_hp_wali">
                                    </div>
                                    <div class="form-group">
                                        <label for="rata_penghasilan_wali"> Rata-Rata Penghasilan Wali per Bulan dari
                                            siswa bersangkutan:</label>
                                        <select parsley-trigger="change" name="rata_penghasilan_wali" id="rata_penghasilan_ortu"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Rata-Rata Penghasilan Wali per Bulan dari siswa
                                                bersangkutan --
                                            </option>
                                            <?php foreach ($penghasilan as $row) {
                                                ?>
                                                <option <?php if ($data['rata_penghasilan_wali'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-section">
                                    <h2>Data Kesehatan Santri</h2>
                                    <div class="form-group">
                                        <label for="kebutuhan_khusus"> Siswa Berkebutuhan Khusus siswa :</label>
                                        <select parsley-trigger="change" name="kebutuhan_khusus" id="kebutuhan_khusus"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['kebutuhan_khusus'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['kebutuhan_khusus'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tuna_rungu"> Tuna Rungu (mengalami hambatan dalam pendengaran)
                                            :</label>
                                        <select parsley-trigger="change" name="tuna_rungu" id="tuna_rungu"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['tuna_rungu'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['tuna_rungu'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tuna_netra"> Tuna Netra (mengalami hambatan dalam penglihatan)
                                            :</label>
                                        <select parsley-trigger="change" name="tuna_netra" id="tuna_netra"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['tuna_netra'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['tuna_netra'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tuna_daksa"> Tuna Daksa (mengalami hambatan secara fisik):</label>
                                        <select parsley-trigger="change" name="tuna_daksa" id="tuna_daksa"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['tuna_daksa'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['tuna_daksa'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tuna_grahita">Tuna Grahita (mengalami hambatan secara mental)
                                            :</label>
                                        <select parsley-trigger="change" name="tuna_grahita" id="tuna_grahita"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['tuna_grahita'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['tuna_grahita'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tuna_laras">Tuna Laras (mengalami hambatan dalam mengendalikan emosi
                                            dan kontrol sosial) :</label>
                                        <select parsley-trigger="change" name="tuna_laras" id="tuna_laras"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['tuna_laras'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['tuna_laras'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="lamban_belajar">Lamban Belajar (mengalami kelambanan dalam memahami
                                            pelajaran dibanding siswa lain) :</label>
                                        <select parsley-trigger="change" name="lamban_belajar" id="lamban_belajar"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['lamban_belajar'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['lamban_belajar'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="sulit_belajar">Sulit Belajar (sering mengalami kesulitan dalam
                                            pelajaran) :</label>
                                        <select parsley-trigger="change" name="sulit_belajar" id="sulit_belajar"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['sulit_belajar'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['sulit_belajar'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="gangguan_komunikasi">Gangguan Komunikasi (mengalami gangguan
                                            komunikasi) :</label>
                                        <select parsley-trigger="change" name="gangguan_komunikasi" id="gangguan_komunikasi"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['gangguan_komunikasi'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['gangguan_komunikasi'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="bakat_luar_biasa"> Bakat Luar Biasa (memiliki suatu bakat luar biasa
                                            yang tidak dimiliki siswa lain) :</label>
                                        <select parsley-trigger="change" name="bakat_luar_biasa" id="bakat_luar_biasa"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih --</option>
                                            <option <?php if ($data['bakat_luar_biasa'] == 1) {
                                                echo "selected";
                                            } ?> value="1">Iya
                                            </option>
                                            <option <?php if ($data['bakat_luar_biasa'] == 0) {
                                                echo "selected";
                                            } ?> value="0">Tidak
                                            </option>
                                        </select>
                                    </div>

                                </div>
                                <div class="form-section">
                                    <h2>Data Prestasi Santri</h2>
                                    <div class="form-group">
                                        <label for="bidang_prestasi"> Bidang Prestasi Tertinggi yang pernah diraih oleh
                                            siswa bersangkutan (dalam 3 tahun terakhir):</label>
                                        <select parsley-trigger="change" name="bidang_prestasi" id="bidang_prestasi"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Bidang Prestasi Tertinggi yang pernah diraih oleh
                                                siswa bersangkutan (dalam 3 tahun terakhir) --
                                            </option>
                                            <?php foreach ($bidangprestasi as $row) {
                                                ?>
                                                <option <?php if ($data['bidang_prestasi'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tingkat_prestasi"> Tingkat Prestasi Tertinggi yang pernah diraih
                                            oleh siswa bersangkutan (dalam 3 tahun terakhir):</label>
                                        <select parsley-trigger="change" name="tingkat_prestasi" id="tingkat_prestasi"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Tingkat Prestasi Tertinggi yang pernah diraih oleh
                                                siswa bersangkutan (dalam 3 tahun terakhir) --
                                            </option>
                                            <?php foreach ($tingkatprestasi as $row) {
                                                ?>
                                                <option <?php if ($data['tingkat_prestasi'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="peringkat_prestasi"> Peringkat Yang Diraih pada Prestasi yang pernah
                                            diraih:</label>
                                        <select parsley-trigger="change" name="peringkat_prestasi" id="peringkat_prestasi"
                                                class="form-control selectpicker"
                                                data-live-search="true">
                                            <option value="">-- Pilih Peringkat Yang Diraih pada Prestasi yang pernah
                                                diraih --
                                            </option>
                                            <?php foreach ($peringkatprestasi as $row) {
                                                ?>
                                                <option <?php if ($data['peringkat_prestasi'] == $row['id']) {
                                                    echo "selected";
                                                } ?> value="<?php echo $row['id'] ?>"><?php echo $row['deskripsi'] ?></option>
                                                <?php
                                            } ?>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="tahun_prestasi">Tahun Memperoleh Prestasi Yang Diraih:</label>
                                        <input type="text" parsley-trigger="change"  class="form-control datepickeryear"
                                               value="<?php echo $data['tahun_prestasi'] ?>"
                                               name="tahun_prestasi"
                                               placeholder="Diisi dengan Tahun Memperoleh Prestasi Yang Diraih Contoh: jika Tahun 2013, maka ditulis 2013."
                                               id="tahun_prestasi">
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_kks">Nomor Kartu Keluarga Sejahtera (KKS) atau Kartu
                                            Perlindungan Sosial (KPS) yang dimiliki orangtua (jika memegang
                                            KKS/KPS):</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nomor_kks'] ?>"
                                               placeholder="Diisi dengan Nomor Kartu Keluarga Sejahtera (KKS) atau Kartu Perlindungan Sosial (KPS) yang dimiliki orangtua (jika memegang KKS/KPS)."
                                               id="nomor_kks" class="form-control"
                                               name="nomor_kks">
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_pkh">Nomor Kartu Program Keluarga Harapan (Kartu PKH) yang
                                            dimiliki orangtua (jika sebagai Peserta PKH):</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nomor_pkh'] ?>"
                                               placeholder="Diisi dengan Nomor Kartu Program Keluarga Harapan (Kartu PKH) yang dimiliki orangtua (jika sebagai Peserta PKH)."
                                               id="nomor_pkh" class="form-control"
                                               name="nomor_pkh">
                                    </div>
                                    <div class="form-group">
                                        <label for="nomor_kip">Nomor Kartu Indonesia Pintar (KIP) yang dimiliki siswa
                                            (diisi hanya bagi siswa pemegang KIP):</label>
                                        <input type="text" parsley-trigger="change"
                                               value="<?php echo $data['nomor_kip'] ?>"
                                               placeholder="omor Kartu Indonesia Pintar (KIP) yang dimiliki siswa (diisi hanya bagi siswa pemegang KIP)."
                                               id="nomor_kip" class="form-control"
                                               name="nomor_kip">
                                    </div>
                                </div>
                                <div class="form-navigation">
                                    <button type="button" class="previous btn btn-info pull-left">&lt; Sebelumnya
                                    </button>
                                    <button type="button" class="next btn btn-info pull-right">Selanjutnya &gt;</button>
                                    <input type="submit" class="btn btn-default pull-right" value="Simpan">
                                    <span class="clearfix"></span>
                                </div>

                            </form>


                        </div>


                    </div>
                    <!-- end row -->


                </div><!-- end col-->

            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo URLS ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/detect.js"></script>
    <script src="<?php echo URLS ?>/assets/js/fastclick.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo URLS ?>/assets/js/waves.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.js"></script>

    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"></script>
    <script type="text/javascript"
            src="<?php echo URLS ?>/assets/plugins/multiselect/js/jquery.multi-select.js"></script>
    <script type="text/javascript"
            src="<?php echo URLS ?>/assets/plugins/jquery-quicksearch/jquery.quicksearch.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/select2/js/select2.min.js" type="text/javascript"></script>
    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-select/js/bootstrap-select.min.js"
            type="text/javascript"></script>
    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js"
            type="text/javascript"></script>
    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"
            type="text/javascript"></script>
    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"
            type="text/javascript"></script>
    <script src="<?php echo URLS ?>/assets/plugins/moment/moment.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/bootstrap-inputmask/bootstrap-inputmask.min.js"
            type="text/javascript"></script>
    <script src="<?php echo URLS ?>/assets/plugins/autoNumeric/autoNumeric.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo URLS ?>/assets/plugins/parsleyjs/parsley.min.js"></script>

    <!-- App js -->
    <script src="<?php echo URLS ?>/assets/js/jquery.core.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        Parsley.addMessages('id', {
            defaultMessage: "tidak valid",
            type: {
                email: "email tidak valid",
                url: "url tidak valid",
                number: "harus angka",
                integer: "harus angka",
                digits: "harus berupa digit",
                alphanum: "harus berupa alphanumeric"
            },
            notblank: "tidak boleh kosong",
            required: "harus di isi",
            pattern: "tidak valid",
            min: "harus lebih besar atau sama dengan %s.",
            max: "harus lebih kecil atau sama dengan %s.",
            range: "harus dalam rentang %s dan %s.",
            minlength: "terlalu pendek, minimal %s karakter atau lebih.",
            maxlength: "terlalu panjang, maksimal %s karakter atau kurang.",
            length: "panjang karakter harus dalam rentang %s dan %s",
            mincheck: "pilih minimal %s pilihan",
            maxcheck: "pilih maksimal %s pilihan",
            check: "pilih antar %s dan %s pilihan",
            equalto: "harus sama"
        });
        $.fn.datepicker.dates['id'] = {
            days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
            daysShort: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
            daysMin: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jum'at", "Sabtu"],
            months: ["Januari", "Februari", "Maret", "April", "May", "Juni", "July", "Agustus", "September", "Oktober", "November", "December"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            today: "Hari ini",
            clear: "kososng",
            format: "mm/dd/yyyy",
            titleFormat: "MM yyyy", /* Leverages same syntax as 'format' */
            weekStart: 0
        };

        Parsley.setLocale('id');

        $(document).ready(function () {
            jQuery('.datepickeryear').datepicker({
                format: "yyyy",
                startView: "years",
                minViewMode: "years",
                autoclose: true,
                todayHighlight: true
            });
            $('.selectpicker').selectpicker();
            $('.autonumber').autoNumeric('init');
            $('#datepicker').datepicker({
                format: "dd/mm/yyyy", language: 'id'

            });
        })
        ;
    </script>
    <script type="text/javascript">
        $(function () {
            var $sections = $('.form-section');

            function navigateTo(index) {
                // Mark the current section with the class 'current'
                $sections
                    .removeClass('current')
                    .eq(index)
                    .addClass('current');
                // Show only the navigation buttons that make sense for the current section:
                $('.form-navigation .previous').toggle(index > 0);
                var atTheEnd = index >= $sections.length - 1;
                $('.form-navigation .next').toggle(!atTheEnd);
                $('.form-navigation [type=submit]').toggle(atTheEnd);
            }

            function curIndex() {
                // Return the current index by looking at which section has the class 'current'
                return $sections.index($sections.filter('.current'));
            }

            // Previous button is easy, just go back
            $('.form-navigation .previous').click(function () {
                navigateTo(curIndex() - 1);
            });

            // Next button goes forward iff current block validates
            $('.form-navigation .next').click(function () {
                if ($('.demo-form').parsley().validate({group: 'block-' + curIndex()}))
                    navigateTo(curIndex() + 1);
            });

            // Prepare sections by setting the `data-parsley-group` attribute to 'block-0', 'block-1', etc.
            $sections.each(function (index, section) {
                $(section).find(':input').attr('data-parsley-group', 'block-' + index);
            });
            navigateTo(0); // Start at the beginning
        });
    </script>
