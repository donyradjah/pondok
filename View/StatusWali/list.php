<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">


            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Status Wali </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Data Master</a>
                            </li>
                            <li class="active">
                                Status Wali
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-sm-12">
                    <div class="card-box table-responsive">
                        <?php
                        if ($success == false) {
                            ?>
                            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">

                                <i class="mdi mdi-block-helper"></i>
                                <strong>Terjadi Kesalahan - </strong> <?php echo $message ?>
                            </div>
                            <?php
                        }
                        ?>
                        <a data-toggle="tooltip" data-placement="top" title=""
                           data-original-title="Tambah Data"
                           class="btn btn-icon waves-effect waves-light btn-primary"
                           href="<?php echo URLS ?>/statuswali/create"><i
                                    class="fa fa-plus"></i></a>
                        <table id="datatable-responsive"
                               class="table table-striped  table-colored table-info dt-responsive nowrap"
                               cellspacing="0"
                               width="100%">
                            <thead>
                            <tr>
                                <th class="col-md-10">Status Wali</th>
                                <th class="col-md-2">Aksi</th>

                            </tr>
                            </thead>
                            <tbody>
                            <?php if (count($data) > 0) {
                                $i = 0;
                                foreach ($data as $row) {
                                    $i++;
                                    ?>
                                    <tr>
                                        <td> <?php echo $row['deskripsi'] ?></td>
                                        <td><a data-toggle="tooltip" data-placement="top" title=""
                                               data-original-title="Ubah Data"
                                               class="btn btn-icon waves-effect waves-light btn-default"
                                               href="<?php echo URLS ?>/statuswali/update/<?php echo $row['id'] ?>"><i
                                                        class="fa fa-pencil"></i></a>
                                        <a data-toggle="tooltip" data-placement="top" title=""
                                               data-original-title="Hapus Data"
                                               class="btn btn-icon waves-effect waves-light btn-danger"
                                               onclick="return confirm('Apa anda yakin akan menghapus data ?');"
                                               href="<?php echo URLS ?>/statuswali/delete/<?php echo $row['id'] ?>"><i
                                                        class="fa fa-trash"></i></a></td>

                                    </tr>
                                <?php }
                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->


    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo URLS ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/detect.js"></script>
    <script src="<?php echo URLS ?>/assets/js/fastclick.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo URLS ?>/assets/js/waves.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.js"></script>

    <script src="<?php echo URLS ?>/assets/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.bootstrap.js"></script>

    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.buttons.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/buttons.bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/jszip.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/pdfmake.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/vfs_fonts.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/buttons.html5.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/buttons.print.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.keyTable.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.responsive.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/responsive.bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.scroller.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.colVis.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/datatables/dataTables.fixedColumns.min.js"></script>

    <!-- init -->
    <script src="<?php echo URLS ?>/assets/pages/jquery.datatables.init.js"></script>

    <!-- App js -->
    <script src="<?php echo URLS ?>/assets/js/jquery.core.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#datatable-responsive').DataTable({
                "language": {
                    "decimal":        "",
                    "emptyTable":     "Data Kososng",
                    "info":           "menampilkan dari _START_ sampai _END_ dari total _TOTAL_ data",
                    "infoEmpty":      "menampilkan dari 0 sampai 0 dari total  0 data",
                    "infoFiltered":   "(menampilkan dari total _MAX_  data)",
                    "infoPostFix":    "",
                    "thousands":      ",",
                    "lengthMenu":     "menampilkan _MENU_ data",
                    "loadingRecords": "Mohon Tunggu...",
                    "processing":     "Sedang memproses ...",
                    "search":         "Pencarian : ",
                    "zeroRecords":    "Tidak ada data yang cocok",
                    "paginate": {
                        "first":      "Pertama",
                        "last":       "Terakhir",
                        "next":       "Selanjutnya",
                        "previous":   "Sebelumnya"
                    },
                    "aria": {
                        "sortAscending":  ": mengaktifkan pengurutan dari data awal ke yang terakhir",
                        "sortDescending": ": mengaktifkan pengurutan dari data akhir ke yang terawal"
                    }
                }
            });

        });


    </script>
