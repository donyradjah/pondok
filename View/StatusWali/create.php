<!-- ============================================================== -->
<!-- Start right Content here -->
<!-- ============================================================== -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">

            <div class="row">
                <div class="col-xs-12">
                    <div class="page-title-box">
                        <h4 class="page-title">Status Wali </h4>
                        <ol class="breadcrumb p-0 m-0">
                            <li>
                                <a href="#">Data Master</a>
                            </li>
                            <li class="active">
                                Status Wali
                            </li>
                        </ol>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!-- end row -->


            <div class="row">
                <div class="col-xs-12">
                    <div class="card-box">


                        <h4 class="header-title m-t-0">Tambah Daftar Status Wali</h4>
                        <?php
                        if ($success == false && isset($success)) {
                            ?>
                            <div class="alert alert-icon alert-danger alert-dismissible fade in" role="alert">

                                <i class="mdi mdi-block-helper"></i>
                                <strong>Terjadi Kesalahan - </strong> <?php echo $message ?>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="p-20">
                            <form method="post" data-parsley-validate novalidate>
                                <div class="form-group">
                                    <label for="deskripsi">Status Wali<span
                                                class="text-danger">*</span></label>
                                    <input type="text" name="deskripsi" parsley-trigger="change" required
                                           placeholder="Nama Status Wali" class="form-control" id="deskripsi"
                                           value="<?php echo $data['deskripsi'] ?>">
                                </div>
                                <div class="form-group text-right m-b-0">
                                    <button class="btn btn-primary waves-effect waves-light" type="submit">
                                        Simpan
                                    </button>
                                    <a href="<?php echo URLS ?>/statussiswa"
                                       class="btn btn-default waves-effect m-l-5">
                                        Batal
                                    </a>
                                </div>

                            </form>

                        </div>


                    </div>
                    <!-- end row -->


                </div><!-- end col-->

            </div>
            <!-- end row -->


        </div> <!-- container -->

    </div> <!-- content -->

    <script>
        var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?php echo URLS ?>/assets/js/jquery.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php echo URLS ?>/assets/js/detect.js"></script>
    <script src="<?php echo URLS ?>/assets/js/fastclick.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.blockUI.js"></script>
    <script src="<?php echo URLS ?>/assets/js/waves.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.slimscroll.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.scrollTo.min.js"></script>
    <script src="<?php echo URLS ?>/assets/plugins/switchery/switchery.min.js"></script>

    <script type="text/javascript" src="<?php echo URLS ?>/assets/plugins/parsleyjs/parsley.min.js"></script>

    <!-- App js -->
    <script src="<?php echo URLS ?>/assets/js/jquery.core.js"></script>
    <script src="<?php echo URLS ?>/assets/js/jquery.app.js"></script>

    <script type="text/javascript">
        Parsley.addMessages('id', {
            defaultMessage: "tidak valid",
            type: {
                email:        "email tidak valid",
                url:          "url tidak valid",
                number:       "nomor tidak valid",
                integer:      "integer tidak valid",
                digits:       "harus berupa digit",
                alphanum:     "harus berupa alphanumeric"
            },
            notblank:       "tidak boleh kosong",
            required:       "tidak boleh kosong",
            pattern:        "tidak valid",
            min:            "harus lebih besar atau sama dengan %s.",
            max:            "harus lebih kecil atau sama dengan %s.",
            range:          "harus dalam rentang %s dan %s.",
            minlength:      "terlalu pendek, minimal %s karakter atau lebih.",
            maxlength:      "terlalu panjang, maksimal %s karakter atau kurang.",
            length:         "panjang karakter harus dalam rentang %s dan %s",
            mincheck:       "pilih minimal %s pilihan",
            maxcheck:       "pilih maksimal %s pilihan",
            check:          "pilih antar %s dan %s pilihan",
            equalto:        "harus sama"
        });

        Parsley.setLocale('id');
        $(document).ready(function () {
            $('form').parsley();
        });

    </script>
